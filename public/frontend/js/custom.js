$(document).ready(function () {

    "use strict";

    $(window).scroll(function () {

        var getWidth = $(window).scrollTop();

        if (getWidth > 70) {

            $('.navbar-fixed-top').addClass("sticky");

// $('.topBar').slideUp();

        } else {

            $('.navbar-fixed-top').removeClass("sticky");

// $('.topBar').slideDown();

        }

    });

    $('.panel-heading a').click(function () {

        $('.panel-heading').removeClass('active');

        if (!$(this).closest('.panel').find('.panel-collapse').hasClass('in')) {

            $(this).parents('.panel-heading').addClass('active');

        }

    });

    jQuery('.panel-title > a').click(function () {

        jQuery(this).find('i').toggleClass('fa-chevron-down fa-chevron-up').closest('panel').siblings('panel').find('i').removeClass('fa-chevron-down').addClass('fa-chevron-up');

    });

    $('#showTextarea').change(function () {

        $('#textareaField').toggle();

    });

    $('.panel-collapse').on('show.bs.collapse', function () {
        $(this).siblings('.panel-heading').addClass('active');
    });

    $('.panel-collapse').on('hide.bs.collapse', function () {
        $(this).siblings('.panel-heading').removeClass('active');
    });

});
