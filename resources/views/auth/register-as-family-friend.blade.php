@extends('frontend.layout.app')
@section('title','Join as Family Friend')

@push('css')
    <style>
        .pb-14 {
            padding-bottom: 14px;
        }

        .width-100 {
            width: 100%;
        }

        .select2-container--default .select2-selection--single {

            width: 100% !important;
            height: 36px !important;
            border: #53417b solid 1.5px !important;
            color: #53417b;
            background: transparent !important;
        }

        span.select2.select2-container.select2-container--default.select2-container--above.select2-container--open {
            width: 100% !important;
        }
        .input-codes {
            width: 24%;
            height: 45px;
            text-align: center;
            font-weight: 500;
            font-size: 22px;
        }
    </style>
@endpush

@section('banner')
    <div class="innerBannerContent fieldsBannerContent">
        <div class="container">
            <h2>Sign Up</h2>
            <div class="row">
                <div class="col-md-8 col-sm-8 col-xs-12">

                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif


                    <form method="POST" action="javascript:void(0)" id="sign-up-user">
                        @csrf
                        <div class="row">
                            <div class="col-md-5 col-sm-8 col-xs-12">
                                <div class="form-group ">
                                    <label for="phone_number">{{ __('Phone Number') }}<span>*</span></label>
                                    <input id="phone_number" type="tel"
                                           class="form-control @error('phone_number') is-invalid @enderror"
                                           name="phone_number" value="{{ old('phone_number') }}" required
                                           maxlength="11" autocomplete="phone_number"
                                           placeholder="03001234567" pattern="[03]{2}[0-9]{9}"
                                           onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')"
                                           autofocus>
                                    @error('phone_number')
                                    <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <button type="submit"
                                            class="btn btn-primary btnMain btnDetails">{{ __('Send') }}</button>
                                </div>
                                <p>Already have a Account? <a href="{{ route('login') }}">Sign In</a></p>
                            </div>
                        </div>
                    </form>
                    <form method="POST" action="javascript:void(0)" id="otp-send" style="display: none">
                        <input type="hidden" value="0" name="mobile_number" id="mobile_number">
                        @csrf
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group OTP-Codes">
                                    <label for="otp_code" style="display: block">{{ __('Enter 4 Digit OTP Number') }}
                                        <span>*</span></label>
                                    <input type="text" class="input-codes" id="otp_code1" name="otp_code1" maxlength="1"/>
                                    <input type="text" class="input-codes" id="otp_code2" name="otp_code2" maxlength="1"/>
                                    <input type="text" class="input-codes" id="otp_code3" name="otp_code3" maxlength="1"/>
                                    <input type="text" class="input-codes" id="otp_code4" name="otp_code4" maxlength="1"/>
                                    <input id="otp_code" type="hidden"
                                           class="form-control @error('otp_code') is-invalid @enderror"
                                           name="otp_code" value="{{ old('otp_code') }}" required
                                           autocomplete="otp_code" autofocus>
                                    @error('otp_code')
                                    <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <button type="submit"
                                            class="btn btn-primary btnMain btnDetails">{{ __('Verify') }}</button>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <p>Already have a Account? <a href="{{ route('login') }}">Sign In</a></p>
                            </div>
                        </div>
                    </form>
                    <form method="POST" action="javascript:void(0)"  id="register_user" style="display: none">
                        @csrf
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12 pb-14">
                                @if ($errors->any())
                                    <p class="btn btn-danger width-100">Please fill all required fields</p>
                                    {{--                    <ul>--}}
                                    {{--                        @foreach ($errors->all() as $error)--}}
                                    {{--                            <li>{{ $error }}</li>--}}
                                    {{--                        @endforeach--}}
                                    {{--                    </ul>--}}
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <input type="hidden" name="role_id" value="2">
                            <div class="col-md-6 col-sm-6 col-xs-12 ">
                                <div class="form-group">
                                    <label for="first_name">First Name<span>*</span></label>
                                    <input id="first_name" type="text"
                                           class="form-control @error('first_name') is-invalid @enderror"
                                           name="first_name" value="{{ old('first_name') }}"
                                           autocomplete="first_name" autofocus>

                                    @error('first_name')
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="last_name">Last Name<span>*</span></label>
                                    <input id="last_name" type="text"
                                           class="form-control @error('last_name') is-invalid @enderror"
                                           name="last_name" value="{{ old('last_name') }}"
                                           autocomplete="last_name" autofocus>

                                    @error('last_name')
                                    <span class="invalid-feedback" role="alert">
                                     <strong>{{ $message }}</strong>
                                </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row">

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label for="email">Email<span>*</span></label>
                                    <input id="email" type="email"
                                           class="form-control @error('email') is-invalid @enderror"
                                           name="email"
                                           value="{{ old('email') }}" autocomplete="email">

                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="category_id">Gender<span>*</span></label>
                                    <select id="category_id"
                                            class="form-control @error('category_id') is-invalid @enderror"
                                            name="category_id" autocomplete="category_id">
                                        @if(!empty($categories))
                                            <option value="">Select</option>
                                            @foreach($categories as $category)
                                                <option
                                                    value="{{$category->id}}" {{ (old('category_id') == $category->id) ? 'selected' : ''}}>{{ucfirst($category->name)}}</option>
                                            @endforeach
                                        @endif
                                    </select>

                                    @error('category_id')
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="age">Age<span>*</span></label>
                                    <select id="age" class="form-control @error('age') is-invalid @enderror"
                                            name="age"
                                            autocomplete="age">
                                        <option value="" {{ (old('age') == '') ? 'selected' : '' }}>Select
                                        </option>
                                        <option
                                            value="Below 18" {{ (old('age') == 'Below 18') ? 'selected' : '' }}>
                                            Below 18
                                        </option>
                                        <option
                                            value="18 - 24" {{ (old('age') == '18 - 24') ? 'selected' : '' }}>18
                                            - 24
                                        </option>
                                        <option
                                            value="25 - 34" {{ (old('age') == '25 - 34') ? 'selected' : '' }}>25
                                            - 34
                                        </option>
                                        <option
                                            value="35 - 44" {{ (old('age') == '35 - 44') ? 'selected' : '' }}>35
                                            - 44
                                        </option>
                                        <option
                                            value="45 - 60" {{ (old('age') == '45 - 60') ? 'selected' : '' }}>45
                                            - 60
                                        </option>
                                        <option value="60+" {{ (old('age') == '60+') ? 'selected' : '' }}>60+
                                        </option>
                                    </select>

                                    @error('age')
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="mobile_number_sign_up">Mobile Number<span>*</span></label>
                                    <input id="mobile_number_sign_up" type="tel"
                                           class="form-control @error('phone_number') is-invalid @enderror"
                                           name="phone_number" value="{{ old('phone_number') }}" readonly
                                           autocomplete="phone_number" autofocus placeholder="03001234567"
                                           pattern="[03]{2}[0-9]{9}"
                                           title="Phone number with 03 and remaing 9 digit with 0-9">

                                    @error('phone_number')
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="cnic">CNIC <span>*</span></label>
                                    <input id="cnic" type="text"
                                           class="form-control cnic-mask @error('cnic') is-invalid @enderror"
                                           name="cnic"
                                           placeholder="_____-_______-_"
                                           value="{{ old('cnic') }}" autocomplete="email">
                                    @error('cnic')
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="password">Password<span>*</span></label>
                                    <input id="password" type="password"
                                           class="form-control @error('password') is-invalid @enderror"
                                           name="password"
                                           autocomplete="new-password">

                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="password-confirm">Confirm Password <span>*</span></label>
                                    <input id="password-confirm" type="password" class="form-control"
                                           name="password_confirmation" autocomplete="new-password">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label for="address">Address<span>*</span></label>
                                    <input id="address" type="text"
                                           class="form-control @error('address') is-invalid @enderror"
                                           name="address"
                                           value="{{ old('address') }}" autocomplete="address">

                                    @error('address')
                                    <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                                </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row">

                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="city_id">City <span>*</span> </label>
                                    <select id="city_id"
                                            class="form-control cities @error('city_id') is-invalid @enderror"
                                            name="city_id" autocomplete="city_id">
                                        <option value="">Select</option>
                                        @if(!empty($cities))
                                            @foreach($cities as $city)
                                                <option
                                                    value="{{$city->id}}" {{ (old('city_id') == $city->id) ? 'selected' : ''}}>{{$city->name}}</option>
                                            @endforeach
                                        @endif
                                    </select>

                                    @error('city_id')
                                    <span class="invalid-feedback" role="alert">
                                  <strong>{{ $message }}</strong>
                                </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label for="area_id">Area <span>*</span></label>
                                    <select id="area_id"
                                            class="form-control areas @error('area_id') is-invalid @enderror"
                                            name="area_id"
                                            autocomplete="area_id">
                                    </select>

                                    @error('area_id')
                                    <span class="invalid-feedback" role="alert">
                                      <strong>{{ $message }}</strong>
                                </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">

                                    <label for="service_category_id.0">{{ __('Services') }}</label>

                                    <select id="service_category_id.0"
                                            class="form-control service_category @error('service_category_id.0') is-invalid @enderror js-example-basic-multiple"
                                            name="service_category_id[]" autocomplete="service_category_id.0"
                                            multiple="multiple">
                                        @foreach($services as $service)
                                            <option value="{{$service->id}}">{{ $service->name }}</option>
                                        @endforeach

                                    </select>

                                    @error('service_category_id.0')
                                    <span class="invalid-feedback" role="alert">
                                      <strong>{{ $message }}</strong>
                                </span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row">


                            <div class="col-md-12">
                                <div class="form-group">
                                    <label><input type="checkbox"> &nbsp; I agree to the terms & services</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary btnMain btnDetails">Submit</button>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('content')


@endsection

@push('js')


<script src="{{ asset('frontend/js/jquery.autotab.js') }}"></script>
<script>
        var previous_value;

        $(document).ready(function () {

        $('#otp_code1').autotab({ target: '#otp_code2', format: 'numeric' });
        $('#otp_code2').autotab({ target: '#otp_code3', format: 'numeric', previous: '#otp_code1' });
        $('#otp_code3').autotab({ target: '#otp_code4', format: 'numeric', previous: '#otp_code2' });
        $('#otp_code4').autotab({ previous: '#otp_code3', format: 'numeric' });

            $('.areas').select2({
                width: '100%',
                maximumInputLength: 20 // only allow terms up to 20 characters long
            });

            $('.service_category').select2({
                width: '100%',
                maximumInputLength: 20 // only allow terms up to 20 characters long
            });

            $('.service_category').trigger('change');
            $('.areas').trigger('change');
            $('.cities').trigger('change');
            setTimeout(function () {
                previous_value = '{{ old('area_id') }}';
                $('#area_id option:selected').val('{{ old('area_id') }}');
            }, 1000);
        });
        $('#cnic').focusout(function () {
            cnic_no_regex = /^[0-9+]{5}-[0-9+]{7}-[0-9]{1}$/;

            if (cnic_no_regex.test($(this).val())) {
            } else {
                toastr['error']("Please enter valid CNIC Number.");
                $(this).val('');
            }
        });

        $('.cities').change(function () {
            form = $(this).closest('form');
            node = $(this);
            node_to_modify = '.areas';
            var city_id = $(this).val();
            var request = "city_id=" + city_id;

            if (city_id !== '') {
                $.ajax({
                    type: "GET",
                    url: "{{ route('ajax.cityAreas') }}",
                    data: request,
                    dataType: "json",
                    cache: true,
                    success: function (response) {
                        if (response.status == "success") {
                            var html = "";
                            $.each(response.data.area, function (i, obj) {
                                html += '<option value="' + obj.id + '" >' + obj.name + '</option>';
                            });
                            $(node_to_modify).html(html);
                            // $(node_to_modify).prepend("<option value='' selected>Select</option>");


                            $('.areas').find('option[value="{{ old('area_id') }}"]').attr('selected', 'selected');
                            $('.areas').trigger('change');

                        }
                    },
                    error: function () {
                        toastr['error']("Something Went Wrong.");
                    }
                });
            } else {
                $(node_to_modify).html("<option value='' selected>Select</option>");
            }
        });

        function getCookie(cname) {
            var name = cname + "=";
            var decodedCookie = decodeURIComponent(document.cookie);
            var ca = decodedCookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') {
                    c = c.substring(1);
                }
                if (c.indexOf(name) == 0) {
                    return c.substring(name.length, c.length);
                }
            }
            return "";
        }


    </script>
    <script>

        /*$(function () {
            'use strict';

            var body = $('body');

            function goToNextInput(e) {
                var key = e.which,
                    t = $(e.target),
                    sib = t.next('input');
                $(this).val($(this).val().replace(/[^\d].+/, ""));
                if ((e.which < 48 || e.which > 57)) {
                    e.preventDefault();
                    return false;
                }

                sib.select().focus();
            }


            function onFocus(e) {
                $(e.target).select();
            }

            body.on('keypress keyup blur', 'input[name=otp_code1]', goToNextInput);
            body.on('keypress keyup blur', 'input[name=otp_code2]', goToNextInput);
            body.on('keypress keyup blur', 'input[name=otp_code3]', goToNextInput);
            body.on('keypress keyup blur', 'input[name=otp_code4]', goToNextInput);
            body.on('click', 'input', onFocus);

        });*/
        $('#sign-up-user').submit(function (e) {
            $('#mobile_number').val($('#phone_number').val());
            e.preventDefault();
            let form = $(this);
            var request = $(form).serialize();
            $.ajax({
                type: "POST",
                url: "{{ route('otp.register.send') }}",
                data: request,
                dataType: "json",
                cache: false,
                success: function (response) {

                    if(response.status == 'success'){
                        toastr['success'](response.message);
                        $('#sign-up-user').hide();
                        $('#otp-send').show();
                    }else{
                        ajaxResponseHandler(response,form);
                    }




                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });

        });
        $('#otp-send').submit(function (e) {
            $('#mobile_number_sign_up').val($('#mobile_number').val());

            $('#otp_code').val($('#otp_code1').val() + '' + $('#otp_code2').val() + '' + $('#otp_code3').val() + '' + $('#otp_code4').val());

            e.preventDefault();
            let form = $(this);
            var request = $(form).serialize();
            $.ajax({
                type: "POST",
                url: "{{ route('verify.otp.register') }}",
                data: request,
                dataType: "json",
                cache: false,
                success: function (response) {
                    // ajaxResponseHandler(response,form);

                    if (response.status == 'success') {
                        $('#sign-up-user').hide();
                        $('#otp-send').hide();
                        $('#register_user').show();
                    } else {
                        ajaxResponseHandler(response,form);
                    }


                },
                error: function (e) {

                    toastr['error'](e.responseJSON.error.otp_code);
                }
            });

        });
        $('#register_user').submit(function (e) {
            $('#mobile_number_sign_up').val($('#mobile_number').val());

            $('#otp_code').val($('#otp_code1').val() + '' + $('#otp_code2').val() + '' + $('#otp_code3').val() + '' + $('#otp_code4').val());

            e.preventDefault();
            let form = $(this);
            var request = $(form).serialize();
            $.ajax({
                type: "POST",
                url: "{{ route('register.client') }}",
                data: request,
                dataType: "json",
                cache: false,
                success: function (response) {

                    if (response.status == 'success') {
                        toastr['success'](response.message);
                        setTimeout(function () {
                            window.location.href = '{{ route('login') }}';
                        }, 2000)
                    } else {
                        ajaxResponseHandler(response,form);
                    }


                },
                error: function (e) {
                    $.each(e.responseJSON.error, function (index, object) {
                        toastr['error'](object);
                    });


                }
            });

        });
    </script>

@endpush
