@php
    $settings =\App\Models\Setting::all();
@endphp

<footer>
    <div class="container">
        <div class="col-md-3 col-sm-6 col-xs-12 footerSocial">
            <img src="{{ asset('frontend/images/logo.png') }}" alt="">
            <div class="row">
                <div class="col-md-12">
                    <ul class="list-unstyled">
                        <li><a href="mailto:{{$settings[1]->value}}">{{$settings[1]->value}}</a></li>
                    </ul>
                    <ul class="list-unstyled list-inline">
                        <li><a href="{{$settings[10]->value}}" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                        <li><a href="{{$settings[9]->value}}" target="_blank"><i class="fab fa-instagram"></i></a></li>
                        <li><a href="{{$settings[8]->value}}" target="_blank"><i class="fab fa-twitter"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-6 quickLinks">
            <h3>Quick Links</h3>
            <ul class="list-unstyled">
                <li><a href="{{ route('index') }}">Home</a></li>
                <li><a href="{{ route('aboutus.detail') }}">About </a></li>
                <li><a href="{{ route('service') }}">Services</a></li>
                <li><a href="{{ route('contacts.create') }}">Contact us</a></li>
            </ul>
        </div>
        <div class="col-md-2 col-sm-6 col-xs-6 quickLinks">
            <h3>Help links</h3>
            <ul class="list-unstyled">
                <li><a href="{{ route('faqs') }}">FAQ</a></li>
                <li><a href="privacy.html">Privacy policy</a></li>
                <li><a href="terms.html">Terms & conditions</a></li>
            </ul>
        </div>
        <div class="col-md-4 col-sm-6 col-xs-12 footerLastArea">
            <div class="col-md-4 col-sm-4 col-xs-4">
                <img src="{{ asset('frontend/images/footerMob.png') }}" alt="">
            </div>
            <div class="col-md-8 col-sm-8 col-xs-8 footerLastAreaImages">
                <h3>Download App <span>Now</span></h3>
                <a href="#" class="applink"><img src="{{ asset('frontend/images/appStore.png') }}" alt="AppStore"></a>
                <a href="#" class="applink"><img src="{{ asset('frontend/images/googlePlay.png') }}" alt="GooglePlay"></a>
            </div>
        </div>
    </div>
</footer>
