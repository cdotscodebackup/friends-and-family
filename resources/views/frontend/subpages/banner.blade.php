@php
    $services = \App\Models\ServiceCategory::take(3)->orderBy('top_service','desc')->get();

@endphp

<div id="myCarousel" class="carousel slide carousel-fade" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        @foreach ($banners as $key => $banner)
            <li data-target="#myCarousel" data-slide-to="{{$key}}" class="{{$key == 0 ? 'active': '' }}"
                style="display: {{ (count($banners) >1) ? '' :'none' }}"></li>
        @endforeach
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">
        @foreach ($banners as $key => $banner)
            <div class="item {{$key == 0 ? 'active': '' }} itemBg"
                 style="background: url({{ asset('uploads/banner/'.$banner->image) }}) no-repeat; background-size: cover; background-position: center;">
                <div class="container">
                    <div class="carousel-caption">
                        <h3>{{ $banner->title }}</h3>
                        <p>{{\Illuminate\Support\Str::limit($banner->description,150)}}</p>
                        <a href="{{$banner->url}}" class="btn btn-primary btnMain">View More</a>
                    </div>
                </div>
            </div>
        @endforeach

    </div>

</div>
