@php
    $categories = \App\Models\Category::with('serviceCategories')->get();
@endphp

<div class="services">
    <div class="container-fluid">
        <h3 class="sectionHeading">Our Services</h3>
        <ul class="nav nav-tabs">
            @foreach($categories as $category)
                <li class="{{ ($category->name == 'Onsite') ? 'active' : '' }}">
                    <a data-toggle="tab" href="#{{ ucfirst($category->name) }}">
                        {{  ucfirst($category->name) }}
                    </a>
                </li>
            @endforeach
        </ul>
        <div class="tab-content">
            @if(!empty($categories))
                @foreach($categories as $category)
                    <div id="{{ucfirst($category->name)}}"
                         class="tab-pane fade in {{ ($category->name == 'Onsite') ? 'active' : '' }}">
                        <div class="servicesMain">
                            <div class="row">
                                @if(!empty($category->serviceCategories))
                                    @foreach($category->serviceCategories as $service)
                                        <div class="col-md-3 col-sm-3 col-xs-12">
                                            <div class="serviceBox">
                                                <a href="{{ route('service.items',[$category->name, $service->slug]) }}">
                                                    <div class="overlay"></div>
                                                    <img
                                                        src="{{$service->thumbnail_image }}"
                                                        alt="{{$service->thumbnail_image}}">
                                                    <div class="serviceContent">
                                                        <div class="serviceIcon">
                                                            <img src="{{$service->icon }}" alt="">
                                                        </div>
                                                        <div class="serviceHeading">
                                                            <h3>{{ $service->name }}</h3>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                    @endforeach
                                @endif

                            </div>
                        </div>
                    </div>
                @endforeach
            @endif

        </div>
    </div>
</div>

@push('js')
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            }
        });

        function getServiceName(name) {
            $.ajax({
                url: "{{ route('getservicesname') }}",
                type: 'POST',
                data: {name: name},
                success: function (response) {
                    if (response.status == 'success') {
                        window.location.href = "{{route('service')}}";
                    } else {
                        toastr['error']("Something went wrong.");
                    }
                },
                error: function (error) {
                    toastr['error']("Something went wrong.");
                }

            });
        }
    </script>
@endpush


