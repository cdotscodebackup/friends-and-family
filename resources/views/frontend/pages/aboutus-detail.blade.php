@extends('frontend.layout.app')
@push('css')
@endpush
@section('banner')
    <div class="innerBannerContent">
        <div class="container">
            <h2>About Us</h2>
            <p>{!! ($aboutus) ? \Illuminate\Support\Str::limit($aboutus->summary,100): ''!!}</p>
        </div>
    </div>

@endsection
@section('content')

    <div class="aboutInner">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <h3 class="sectionHeading">A Great Place to Work. A Great Place to Receive Care.</h3>
                    <p class="abouSectionPara">{!! ($aboutus) ? $aboutus->description : '' !!}</p>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <img src="{{ asset('frontend/images/aboutImg.png') }}" alt="">
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
@endpush

