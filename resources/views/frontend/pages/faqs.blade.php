@extends('frontend.layout.app')
@push('css')
@endpush
@section('banner')
    <div class="innerBannerContent">
        <div class="container">
            <h2>FAQ</h2>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
        </div>
    </div>

@endsection
@section('content')
    <div class="aboutInner">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <h3 class="sectionHeading">Have Any Questions ?</h3>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been
                        the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley
                        of type and scrambled it to make a type specimen book. It has survived not only five centuries,
                        but also the leap into electronic typesetting, remaining essentially unchanged. It was
                        popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages,
                        and more recently with desktop publishing software like Aldus PageMaker including versions of
                        Lorem Ipsum. is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been
                        the industry's standard dummy text. </p>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12" style="margin-top:75px;">
                    <div class="panel-group" id="accordion">

                        @foreach($faqs as $key =>$faq)
                            <div class="panel panel-default">
                                <div class="panel-heading {{ ($loop->index == 0) ?'active' : '' }}">
                                    <h4 class="panel-title">
                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion"
                                           href="#collapse{{$key}}">
                                            <span>?</span> {{ $faq->title }}
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapse{{$key}}"
                                     class="panel-collapse collapse {{ ($loop->index == 0) ?'in' : '' }}">
                                    <div class="panel-body">
                                        {{ $faq->description }}
                                    </div>
                                </div>
                            </div>
                        @endforeach

                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

@push('js')
@endpush
