@extends('frontend.layout.app')
@section('title','Orders')
@push('css')
    <style>
    </style>
@endpush
@section('banner')
    <div class="innerBannerContent">

        <div class="container">


            <h2>{{ $menuItem->name }}</h2>

            <p> {{\Illuminate\Support\Str::limit($menuItem->description,130)}} </p>


        </div>

    </div>
@endsection
@section('content')
    <div class="aboutInner serviceInner">

        <div class="container">

            <div class="row">

                <div class="col-md-12 col-sm-12 col-xs-12">

                    <h3 class="sectionHeading">Order Review</h3>

                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 appointmentLeft orderReviewLeft">

                    <div class="row">

                        <div class="col-md-4 col-sm-4 col-xs-12">

                            <img src="{{ $menuItem->image }}" alt="">

                        </div>

                        <div class="col-md-8 col-sm-8 col-xs-12">

                            <h4>{{ $menuItem->name }}</h4>

                            <p> {{\Illuminate\Support\Str::limit($menuItem->description,130)}} </p>

                        </div>

                    </div>

                    <hr style="border-bottom: #afafaf solid 1px;">

                    <div class="personalInfo">

                        <h3>Personal information</h3>

                        <div class="row">

                            <div class="col-md-12 form-group">

                                <h5>{{ $user->fullName() }}</h5>

                                <h5>Contact No. <span>{{ $user->phone_number }}    </span></h5>

                                <h5>Email <span>{{ $user->email }}</span></h5>

                                <h5>Address <span>{{ $user->address }}</span>
                                </h5>

                            </div>

                            <div class="col-md-12">

                                <label><input type="checkbox" id="showTextarea"> &nbsp; Alternate address for this
                                    service?</label>

                                <textarea style="display:none;" id="textareaField" name="alternate-address"
                                          class="form-control" rows="3"> </textarea>

                            </div>

                        </div>

                    </div>

                </div>

                <div class="col-md-6 col-sm-6 col-xs-12 appointmentRight">

                    <label>Appointment type: ({{ $category->name }})</label>

                    <div class="row">

                        <div class="col-md-12 form-group">

                            <p class="outputRes">Time: <span id="time">7 PM</span></p>

                            <p class="outputRes">Date: <span id="date">18/04/2020</span></p>

                        </div>

                    </div>

                    <div class="row">

                        <div class="col-md-12 form-group">

                            <p class="outputRes">Payment option: </p>

                            <p class="outputRes"><span id="paymentMode">Cash</span><span> on Delivery</span></p>

                        </div>

                    </div>
                    <div class="row">
                        <div class="form-group">
                            <div class="col-md-12 text-right editIcon">
                                <a
                                    href="{{ route('appointment',[$category->name,$service->slug,$menuItem->id]) }}"><img
                                        src="{{ asset('frontend/images/editIcon.png') }}"
                                        alt="" style="width: 5%;
    margin-bottom: 10px;"></a>
                            </div>
                        </div>
                    </div>


                    <div class="appointmentCalculations">


                        <div class="row">

                            <div class="col-md-6 col-sm-6 col-xs-6">

                                <h5>{{ $menuItem->name }}</h5>

                            </div>

                            <div class="col-md-6 col-sm-6 col-xs-6 text-right">

                                <h5><span>PKR <span id="price">{{ number_format($menuItem->price)}}</span>/-</span></h5>

                            </div>

                        </div>

                        <div class="row">

                            <div class="col-md-6 col-sm-6 col-xs-6">

                                <h5>Delivery charges</h5>

                            </div>

                            <div class="col-md-6 col-sm-6 col-xs-6 text-right">

                                <h5><span>PKR <span
                                            id="delivery-charges">{{ ($category->name == 'Online') ? 0 : $user->area->price }}</span>/-</span>
                                </h5>

                            </div>

                        </div>

                        <div class="row totalRow">

                            <div class="col-md-6 col-sm-6 col-xs-6">

                                <h5>Total</h5>

                            </div>

                            <div class="col-md-6 col-sm-6 col-xs-6 text-right">

                                <h5><span>PKR <span id="total-price">{{ number_format($menuItem->price) }}</span>/-</span></h5>

                            </div>

                            <div class="clearfix"></div>

                        </div>

                        <div class="row totalRow">

                            <div class="col-md-12 col-sm-12 col-xs-12 text-right">

                                <img src="" id="paymentSlip" style="display: none">

                            </div>

                            <div class="clearfix"></div>

                        </div>


                    </div>

                    <div class="row">

                        <div class="col-md-6 col-sm-6 col-xs-12">

                            <div class="form-group">
                                <button type="button" data-toggle="modal" data-target="#exampleModal"
                                        class="btn btn-primary btnMain btnDetails"><strong>Cancel</strong>
                                </button>

                                {{--                                <button type="button" class="btn btn-primary btnMain btnDetails">Cancel</button>--}}

                            </div>

                        </div>

                        <div class="col-md-6 col-sm-6 col-xs-12">

                            <div class="form-group">

                                <button type="button" class="btn btn-primary btnMain btnDetails" onclick="saveOrder()">
                                    Submit
                                </button>


                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>
@endsection
@push('models')
    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Cancel Order</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row text-center p-3 f-13">
                        <span>Are you sure you want to cancel the order?</span>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn buttonMain hvr-bounce-to-right" data-dismiss="modal">Close</button>
                    <a href="#" class="btn buttonMain hvr-bounce-to-right" id="order-cancel">Yes</a>
                </div>
            </div>
        </div>
    </div>
@endpush
@push('js')
    <script>
        $(document).ready(function () {
            var finalOrder = JSON.parse(localStorage.getItem('services'));
            $('#time').text(finalOrder.time);
            $('#date').text(finalOrder.requestedDatetime);
            $('#total-price').text(parseInt(finalOrder.totalPrice) + parseInt($('#delivery-charges').text()));
            $('#paymentMode').text(finalOrder.payment_mode);
            if (finalOrder.duration == "0") {
                $('#price').text(finalOrder.price * 1);
            } else {
                $('#price').text(finalOrder.price * finalOrder.duration);
            }


            if (finalOrder.payment_mode == 'Cash') {

            } else {

                bannerImg = document.getElementById('paymentSlip');
                bannerImg.src = finalOrder.payment_slip;
                $('#paymentSlip').show();
            }


        });


        function saveOrder() {


            var get_services = JSON.parse(localStorage.getItem('services'));
            var alternativeAddress = null;
            if ($('#showTextarea').is(':checked')) {
                alternativeAddress = $('#textareaField').val();
            } else {
                alternativeAddress = "";
            }
            if (get_services != null) {
                var newServices = {
                    alternateAddress: alternativeAddress,
                    category_id: get_services.category_id,
                    duration: get_services.duration,
                    menu_item_id: get_services.menu_item_id,
                    name: get_services.name,
                    order_type: get_services.order_type,
                    payment_mode: get_services.payment_mode,
                    payment_slip: get_services.payment_slip,
                    price: get_services.price,
                    requestedDatetime: get_services.requestedDatetime,
                    service_id: get_services.service_id,
                    time: get_services.time,
                    timeZone: Intl.DateTimeFormat().resolvedOptions().timeZone,
                    totalPrice: get_services.totalPrice,
                    service_type: get_services.service_type,

                };
                localStorage.setItem('services', JSON.stringify(newServices));


                var finalOrder = JSON.parse(localStorage.getItem('services'));


                if (finalOrder != null) {
                    $.ajax({
                        type: "POST",
                        url: "{{ route('order.final') }}",
                        data: {
                            'finalOrder': finalOrder,
                            "_token": "{{ csrf_token() }}",
                        },
                        dataType: "json",
                        cache: false,
                        success: function (response) {

                            toastr[response.data.flash_status](response.data.flash_message);

                            if (response.data.flash_status == "success") {
                                localStorage.removeItem('services');
                                window.location = "{{ route('customer.user.order.show', ['id' => "_ORDER_ID_"]) }}".replace('_ORDER_ID_', response.data.order_id);
                            }
                        },
                        error: function () {
                            toastr['error']("Something Went Wrong.");
                        }
                    });
                } else {
                    toastr['error']("You didn't have selected any item yet.");
                }

            }
        }

        $('#order-cancel').click(function () {
            localStorage.clear();
            window.location.href = '{{ route('service') }}';
        });
    </script>

@endpush
