@extends('frontend.layout.app')
@section('title',$service_detail->meta_title ?? 'Services')
@section('description',$service_detail->meta_description ?? 'Services')
@section('keywords',$service_detail->keywords ?? 'Services')

@push('css')
@endpush
@section('banner')
    <div class="innerBannerContent">
        <div class="container">
            <h2>OUR SERVICES</h2>
            <p>{{ $service_detail->summary }}</p>
        </div>
    </div>
@endsection
@section('content')
    <div class="aboutInner serviceInner">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <h3 class="sectionHeading">{{ $service_detail->name }}</h3>
                    <p>{{ $service_detail->description }}</p>
                </div>

            </div>
        </div>
        <div class="container-fluid">
            <div class="servicesMain">
                <div class="row">
                    @if(count($service_detail->sub_categories) > 0)
                        @foreach($service_detail->sub_categories as $key=>$sub_category)
                            <div class="col-md-2 col-sm-3 col-xs-12">
                                <div class="serviceBox serviceBox2">
                                    <div class="serviceInnerContent">
                                        <div class="serviceHeading">
                                            <h3> {{ $sub_category->name }}</h3>
                                            <div class="serviceInnerDet">
                                                <p> {{\Illuminate\Support\Str::limit($sub_category->description,50)}}</p>
                                                <h4>Rs. {{ number_format($sub_category->price)}}
                                                    <span> {{($sub_category->service_type == 'hourly') ? 'Per Service' :''}}</span>
                                                </h4>
                                                @if(auth()->check() && auth()->user()->user_type  == 'customer')
                                                    <a href="{{route('appointment',['category'=>$category->name,'slug'=>$service_detail->slug,'subcategory'=>$sub_category->id])}}"
                                                       class="btn btn-primary btnMain">Schedule Now</a>
                                                @elseif(auth()->check() && auth()->user()->user_type  == 'admin')
                                                @elseif(auth()->check() && auth()->user()->user_type  == 'staff')
                                                @else
                                                    <a href="{{route('login')}}"
                                                       class="btn btn-primary btnMain">Schedule Now</a>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @endif

                </div>
            </div>
        </div>
    </div>
@endsection
@push('js')
    <script>

        function saveOrder(categoryId, serviceId, subCategoryId) {

            var request = {
                "service_id": serviceId,
                "sub_category_id": subCategoryId,
            };

            {{--$.ajax({--}}
            {{--    type: "GET",--}}
            {{--    url: "{{ route('appointment') }}",--}}
            {{--    data: request,--}}
            {{--    dataType: "json",--}}
            {{--    cache: true,--}}
            {{--    success: function (response) {--}}

            {{--        if (response.flash_status == "success") {--}}
            {{--            window.location.href = '{{ route('appointment') }}';--}}
            {{--        } else {--}}
            {{--            toastr[response.flash_status](response.flash_message);--}}
            {{--        }--}}
            {{--    },--}}
            {{--    error: function () {--}}
            {{--        toastr['error']("Something Went Wrong.");--}}
            {{--    }--}}
            {{--});--}}

        }
    </script>

@endpush
