<!DOCTYPE html>
<html lang="en">
<head>
    <link href="{{ asset('frontend/css/bootstrap.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('frontend/css/custom.css') }}">
    <link rel="stylesheet" href="{{ asset('frontend/css/media.css') }}">

    <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@300;400;600;700;900&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Raleway:wght@300;400;500;600;700;900&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('frontend/css/toastr.min.css') }}">
</head>
<body>
<div class="banner innerBanner" style="height: 100vh;">
    <div class="container">
        <div class="mobileContainer col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-12">
            <div class="servicesPage fieldsBannerContent ">
                <form method="POST">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Username</label>
                            <input type="text" id="txtusername" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Password</label>
                            <input type="password" id="txtpassword" class="form-control"> 
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <button type="button" id="btnsave" class="btn btn-block btn-primary btnMain btnDetails" style="width: 100%">Save</button>
                        </div>
                    </div>
                   
                </form>
            </div>
        </div>
    </div>
</div>

<script src="{{ asset('frontend/js/jquery-1.11.3.min.js') }}"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="{{ asset('frontend/js/bootstrap.js') }}"></script>
<!-- Toastr -->
<script src="{{ asset('frontend/js/toastr.min.js') }}"></script>
<script src="{{ asset('frontend/js/custom.js') }}"></script>
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': '{{ csrf_token() }}'
        }
    });
    $("#btnsave").click(function(){
        if($("#txtusername").val() == "familyfriend" && $("#txtpassword").val() == "familyfriend??123"){
            $.ajax({
                type: "POST",
                url: "{{ route('ajax.authcheck') }}",
                data:{check: true},
                dataType: "json",
                cache: true,
                success: function (response) {
                    if (response.status == true) {
                    var url = '{{route("index")}}';
                    window.location.href=url;
                    }
                }
            });
        }
        else{
            toastr['error']("Something Went Wrong.");
        }
    })
</script>



</body>
</html>
