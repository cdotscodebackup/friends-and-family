@extends('customer.main')
@section('title','Notification')
@push('css')
    <style>
        .map-image {
            height: 60px;
            width: 60px;
            border-radius: 50px !important;
            margin-right: 15px;
            float: left;
        }

        .mw-100 {
            margin: 0 auto;
        }

    </style>
@endpush
@section('content')
    <div class="m-content">
        <!--Begin::Section-->
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Notification
                        </h3>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">
                {{--tableScroll--}}
                <table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
                    <thead>
                    <tr>
                        <th> Sr#</th>
                        <th> Type</th>
                        <th> Message</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(!empty($notifications))
                        @foreach($notifications as $key=>$notification)
                            <tr>
                                <td>{{$notification->id}}</td>
                                <td>{{ $notification->message_type  }}</td>
                                <td>{{ $notification->message  }}</td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>

            </div>
        </div>
    </div>
@endsection
@push('js')

    <script>


        var table = $("#m_table_1").DataTable({
            "order": [[0, "desc"]],
            "columnDefs": [
                {orderable: false, targets: [7]}
            ],
        });

    </script>

@endpush