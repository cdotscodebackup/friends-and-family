@extends('service-provider.main')
@section('title','Appointments')
@push('css')
    <style>
        .serviceInner {
            height: 280px;
        }

        .bannerFields {
            margin-top: 15px;
            margin-bottom: 30px;
        }

        .orderReviewDetails p {
            margin: 0;
        }

        .selectedServices {
            border: #ff6c2b solid 2px;
            padding: 15px 15px;
        }

        .btn:not(.btn-sm):not(.btn-lg) {
            line-height: 1.44;
            background-color: #ff6c2b;
        }

        .serviceBoxHeader {
            color: #fff;
        }

        /* Rating Star Widgets Style */
        .rating-stars {
            width: 100%;
        }

        .rating-stars ul {
            list-style-type: none;
            padding: 0;
            margin-top: 30px;

            -moz-user-select: none;
            -webkit-user-select: none;
        }

        .rating-stars ul > li.star {
            display: inline-block;

        }

        /* Idle State of the stars */
        .rating-stars ul > li.star > i.fa {
            font-size: 18px; /* Change the size of the stars */
            color: #ccc; /* Color on idle state */
        }

        /* Hover state of the stars */
        .rating-stars ul > li.star.hover > i.fa {
            color: #FFCC36;
        }

        /* Selected state of the stars */
        .rating-stars ul > li.star.selected > i.fa {
            color: #FF912C;
        }

        .ratingArea {
            margin-top: 30px;
        }

        .ratingArea h4 {
            font-weight: 700;
            margin: 10px 0;
            font-size: 16px;
        }

        .ratingArea img {
            object-fit: cover;
            width: 15%;
        }

        .modal .modal-content .modal-header {
            background: #ff6c2b;
        }

        .modal .modal-content .modal-header .modal-title {
            color: #fff;
        }

        @media screen and (max-width: 450px) {
            .orderReviewDetails h4 {
                margin: 5px 0;
            }
        }

        .mr-10 {
            margin-right: 10px;
        }

        .mb-15 {
            margin-bottom: 15px;
        }


    </style>
@endpush
@section('content')
    <div class="m-content">
        <div class="m-portlet m-portlet--mobile">
            <div class="">
                <div class="">
                    <div class="m-portlet">
                        <div class="container">
                            <div class="m-portlet__body">
                                <ul class="nav nav-pills">
                                    <li class="active btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air mr-10">
                                        <a href="#tab_2_1" data-toggle="tab" aria-expanded="true" class="text-white">
                                            Customer Bill </a>
                                    </li>
                                    <li class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air mr-10">
                                        <a href="#tab_2_3" data-toggle="tab" aria-expanded="false" class="text-white">
                                            Service Provider Bill </a>
                                    </li>

                                </ul>

                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab_2_1">
                                        <div class="container">
                                            <div class="serviceInnerMain">

                                                <div class="serviceBoxMain">
                                                    <div class="serverInnerDetails">
                                                        <div class="row">
                                                            <div class="col-md-12 col-sm-12 col-xs-12 bannerFields">
                                                                <div class="row">
                                                                    <div
                                                                        class="col-md-12 col-sm-12 col-xs-12 orderReviewImage">

                                                                        @if($order->order_status == 'completed' && $order->staff_status == 'completed')
                                                                        @else
                                                                            <p>Thank you for placing an appointment with
                                                                                Friends &
                                                                                Family!
                                                                                Someone will be in touch with you
                                                                                shortly to
                                                                                confirm your service order.</p>
                                                                        @endif
                                                                    </div>
                                                                    <div
                                                                        class="col-md-10 col-sm-10 col-xs-12 orderReviewDetails">
                                                                        <div class="row">
                                                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                                                <h4>
                                                                                    <b>{{ $order->user->fullName() }} </b><br>
                                                                                </h4>
                                                                                <p>{{ $order->phone_number }}</p>
                                                                                <p>
                                                                                    <strong>Date/Time: </strong> {{\Carbon\Carbon::parse($order->order_details[0]->date)->setTimezone($order->time_zone)->format('m/d/Y')}}  {{\Carbon\Carbon::parse($order->order_details[0]->time)->format('h:i A')}}
                                                                                </p>

                                                                                <p><strong>Appointment
                                                                                        Type: </strong>{{ ucfirst($order->order_details[0]->category->name)  }}
                                                                                </p>
                                                                            </div>
                                                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                                                <h4><b>Address</b></h4>
                                                                                <p>{{  $order->user->address }}
                                                                                    , {{ $order->area->name }}
                                                                                    , {{ $order->city->name }}</p>
                                                                            </div>
                                                                            <div class="col-md-4 col-sm-4 col-xs-12">


                                                                                @if(!is_null($order->order_details[0]->alternate_address ))
                                                                                    <h4><b>Alternate Address</b></h4>
                                                                                    <p>{{ $order->order_details[0]->alternate_address  }}</p>
                                                                                @endif
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div
                                                                class="col-md-12 col-sm-12 col-xs-12 selectedServicesRight selectedServicesRight2 ">
                                                                <h4 class="selectedServicesRightHeading"><b>Services
                                                                        Ordered</b>
                                                                </h4>
                                                                <div class="selectedServices">
                                                                    @if(!empty($order->order_details))
                                                                        @foreach($order->order_details as $order_details)
                                                                            <div class="row">
                                                                                <div
                                                                                    class="col-md-12 col-sm-12 col-xs-12">
                                                                                    <div class="row">
                                                                                        <div
                                                                                            class="col-md-6 col-sm-6 col-xs-12">
                                                                                            <h4>{{ $order_details->name }}</h4>
                                                                                        </div>
                                                                                        <div
                                                                                            class="col-md-6 col-sm-6 -col-xs-12 text-right">
                                                                                            <h4>
                                                                                                Rs. {{ $order_details->amount }}</h4>
                                                                                        </div>
                                                                                        <div class="clearfix"></div>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="clearfix"></div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <hr style="margin: 10px 0; border: #ff6c2b dotted 1px; width: 100%;">
                                                                            </div>
                                                                        @endforeach
                                                                        <div class="row delivery-charges">

                                                                            <div
                                                                                class="col-md-12 col-sm-12 col-xs-12 text-left">
                                                                                <h5>Delivery Charges:
                                                                                    <span style="float: right">Rs. <span
                                                                                            id="delivery-charges-price">{{ (int)$order->delivery_charges }}</span></span>
                                                                                </h5>

                                                                            </div>
                                                                            @if($order->order_details[0]->type == 'Hourly')
                                                                                <div
                                                                                    class="col-md-12 col-sm-12 col-xs-12 text-left">
                                                                                    <h5>Appointment Duration:
                                                                                        <span style="float: right"><span
                                                                                                id="appoitment-duration">{{ intdiv($order->order_duration, 60).' hours '. ($order->order_duration % 60) }} minutes</span></span>
                                                                                    </h5>

                                                                                </div>
                                                                            @endif
                                                                            <div class="clearfix"></div>
                                                                        </div>
                                                                    @endif
                                                                </div>
                                                                <div class="serviceBoxHeader">
                                                                    <div class="row">
                                                                        <div
                                                                            class="col-md-12 col-sm-12 col-xs-12 text-left">
                                                                            <h5><strong>Grand Total</strong>
                                                                                <span style="float: right">
                                                                            <strong>Rs. <span
                                                                                    id="delivery-charges-price">{{ (int)$order->total_price + (int)$order->delivery_charges ?? 0 }}</span></strong>
                                                                        </span>
                                                                            </h5>
                                                                            <h5><strong> Paid Amount</strong>
                                                                                <span style="float: right">
                                                                            <strong>Rs. <span
                                                                                    id="delivery-charges-price">{{ (int)$total_paid_amount }}</span></strong>
                                                                        </span>
                                                                            </h5>
                                                                            <h5><strong>Payable Amount</strong>
                                                                                <span style="float: right">
                                                                            <strong>Rs. <span
                                                                                    id="delivery-charges-price">{{ (int)$order->total_price - (int)$total_paid_amount }}</span></strong>
                                                                        </span>
                                                                            </h5>
                                                                        </div>
                                                                        <div class="clearfix"></div>
                
                                                                        
                                                                    </div>
                                                                </div>

                                                                <br>
                                                                @if($order->order_status == "completed" && $order->staff_status == "completed")
                                                                    @else
                                                                <div class="clearfix"></div>
                                                                <div class="row">
                                                                    <div class="col-md-12 col-sm-12 col-xs-12 text-left">
                                                                    
                                                                    <button type="button" class="btn btn-info" onclick="orderProgress('{{$order->id}}','collect-cash')">Complete</button>
                                                                    
                                                                </div>
                                                                </div>
                                                                @endif
                                                            </div>
                                                        </div>
                                                        @if($order->order_status == 'confirmed')
                                                            @if(!is_null($order->staff))

                                                                <form action="#" method="post" id="submit-rating"
                                                                      enctype="multipart/form-data">
                                                                    @csrf
                                                                    <div class="row ratingArea disabledDiv">
                                                                        <div class="col-md-12">
                                                                            <h4>Service Provider</h4>
                                                                            <img width="300" height="200"
                                                                                 class="img-thumbnail"
                                                                                 src="{{ asset('uploads/user_profiles/'.$order->staff->profile_pic) }}"
                                                                                 alt="">
                                                                            <p>{{ $order->staff->fullName() }}</p>
                                                                            <p>{{ $order->staff->phone_number }}</p>
                                                                        </div>


                                                                    </div>
                                                                </form>
                                                            @endif
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="tab-pane" id="tab_2_3">
                                        <div class="container">
                                            <div class="serviceInnerMain">

                                                <div class="serviceBoxMain">
                                                    <div class="serverInnerDetails">
                                                        <div class="row">
                                                            <div class="col-md-12 col-sm-12 col-xs-12 bannerFields">
                                                                <div class="row">
                                                                    <div
                                                                        class="col-md-10 col-sm-10 col-xs-12 orderReviewDetails">
                                                                        <div class="row">
                                                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                                                <h4>
                                                                                    <b>{{ $order->user->fullName() }} </b><br>
                                                                                </h4>
                                                                                <p>{{ $order->phone_number }}</p>
                                                                                <p>
                                                                                    <strong>Date/Time: </strong> {{\Carbon\Carbon::parse($order->order_details[0]->date)->setTimezone($order->time_zone)->format('m/d/Y')}}  {{\Carbon\Carbon::parse($order->order_details[0]->time)->format('h:i A')}}
                                                                                </p>

                                                                                <p><strong>Appointment
                                                                                        Type: </strong>{{ ucfirst($order->order_details[0]->category->name)  }}
                                                                                </p>
                                                                            </div>
                                                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                                                <h4><b>Address</b></h4>
                                                                                <p>{{  $order->user->address }}
                                                                                    , {{ $order->area->name }}
                                                                                    , {{ $order->city->name }}</p>
                                                                            </div>
                                                                            <div class="col-md-4 col-sm-4 col-xs-12">


                                                                                @if(!is_null($order->order_details[0]->alternate_address ))
                                                                                    <h4><b>Alternate Address</b></h4>
                                                                                    <p>{{ $order->order_details[0]->alternate_address  }}</p>
                                                                                @endif
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div
                                                                class="col-md-12 col-sm-12 col-xs-12 selectedServicesRight selectedServicesRight2 ">
                                                                <h4 class="selectedServicesRightHeading"><b>Services
                                                                        Ordered</b>
                                                                </h4>
                                                                <div class="selectedServices">
                                                                    @if(!empty($order->order_details))
                                                                        @foreach($order->order_details as $order_details)
                                                                            <div class="row">
                                                                                <div
                                                                                    class="col-md-12 col-sm-12 col-xs-12">
                                                                                    <div class="row">
                                                                                        <div
                                                                                            class="col-md-6 col-sm-6 col-xs-12">
                                                                                            <h4>{{ $order_details->name }}</h4>
                                                                                        </div>
                                                                                        <div
                                                                                            class="col-md-6 col-sm-6 -col-xs-12 text-right">
                                                                                            <h4>
                                                                                                Rs. {{ $order_details->amount }}</h4>
                                                                                        </div>
                                                                                        <div class="clearfix"></div>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="clearfix"></div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <hr style="margin: 10px 0; border: #ff6c2b dotted 1px; width: 100%;">
                                                                            </div>
                                                                        @endforeach
                                                                        <div class="row delivery-charges">

                                                                            <div
                                                                                class="col-md-12 col-sm-12 col-xs-12 text-left">
                                                                                <h5>Delivery Charges:
                                                                                    <span style="float: right">Rs. <span
                                                                                            id="delivery-charges-price">{{ (int)$order->delivery_charges }}</span></span>
                                                                                </h5>

                                                                            </div>
                                                                            @if($order->order_details[0]->type == 'Hourly')
                                                                                <div
                                                                                    class="col-md-12 col-sm-12 col-xs-12 text-left">
                                                                                    <h5>Appointment Duration:
                                                                                        <span style="float: right"><span
                                                                                                id="appoitment-duration">{{ intdiv($order->order_duration, 60).' hours '. ($order->order_duration % 60) }} minutes</span></span>
                                                                                    </h5>

                                                                                </div>
                                                                            @endif
                                                                            @if(!is_null($order->staff->comission))
                                                                                <div
                                                                                    class="col-md-12 col-sm-12 col-xs-12 text-left">
                                                                                    <h5>Company Share:
                                                                                        <span
                                                                                            style="float: right">Rs. <span
                                                                                                id="company-share">{{(int)($order->total_price - ($order->total_price*$order->staff->comission->company_comission)/100)  }}</span></span>
                                                                                    </h5>
                                                                                </div>
                                                                                <div
                                                                                    class="col-md-12 col-sm-12 col-xs-12 text-left">
                                                                                    <h5>Service Provider Share:
                                                                                        <span
                                                                                            style="float: right">Rs. <span
                                                                                                id="sp-share">{{(int)($order->total_price - ($order->total_price * $order->staff->comission->staff_comission)/100)  }}</span></span>
                                                                                    </h5>
                                                                                </div>
                                                                            @endif
                                                                            <div class="clearfix"></div>
                                                                        </div>
                                                                    @endif
                                                                </div>
                                                                <div class="serviceBoxHeader">
                                                                    <div class="row">
                                                                        <div
                                                                            class="col-md-12 col-sm-12 col-xs-12 text-left">
                                                                            <h5><strong>Grand Total</strong>
                                                                                <span style="float: right">
                                                                            <strong>Rs. <span
                                                                                    id="delivery-charges-price">{{ (int)$order->total_price + (int)$order->delivery_charges ?? 0 }}</span></strong>
                                                                        </span>
                                                                            </h5>

                                                                        </div>
                                                                        <div class="clearfix"></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        @if($order->order_status == 'confirmed')
                                                            @if(!is_null($order->staff))

                                                                <form action="#" method="post" id="submit-rating"
                                                                      enctype="multipart/form-data">
                                                                    @csrf
                                                                    <div class="row ratingArea disabledDiv">
                                                                        <div class="col-md-12">
                                                                            <h4>Service Provider</h4>
                                                                            <img width="300" height="200"
                                                                                 class="img-thumbnail"
                                                                                 src="{{ asset('uploads/user_profiles/'.$order->staff->profile_pic) }}"
                                                                                 alt="">
                                                                            <p>{{ $order->staff->fullName() }}</p>
                                                                            <p>{{ $order->staff->phone_number }}</p>
                                                                        </div>


                                                                    </div>
                                                                </form>
                                                            @endif
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>


                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('js')
    <script>
        function orderProgress(orderId, orderProgressStatus) {
            var order_id = parseInt(orderId);
            var request = {
                "order_id": order_id,
                "order_progress_status": orderProgressStatus,
                "_token": "{{ csrf_token() }}"
            };
            if (order_id !== '') {
                $.ajax({
                    type: "POST",
                    url: "{{ route('staff.update.order.progress.status') }}",
                    data: request,
                    dataType: "json",
                    cache: true,
                    success: function (response) {
                        if (response.status == "success") {
                            toastr[response.status](response.message);
                            setTimeout(function () {
                                window.location.reload();
                            },2000);
                        }
                    },
                    error: function () {
                        toastr['error']("Something Went Wrong.");
                    }
                });
            }
        }
    </script>
@endpush

