@extends('service-provider.main')
@section('title','Wallet')
@push('css')
    <style>
        .map-image {
            height: 60px;
            width: 60px;
            border-radius: 50px !important;
            margin-right: 15px;
            float: left;
        }

        .mw-100 {
            margin: 0 auto;
        }

    </style>
@endpush
@section('content')
    <div class="m-content">
        <!--Begin::Section-->
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Wallet
                        </h3>
                    </div>
                </div>
                <div class="m-portlet__head-tools">
                    <ul class="m-portlet__nav">
                        <li class="m-portlet__nav-item">
                            <a data-target="#payment" data-toggle="modal"
                               class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                                <span>
                                    <i class="la la-plus"></i>
                                    <span>Payment</span>
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="m-portlet__body">
                {{--tableScroll--}}
                <h5>Total Recived Amount : <span>{{$provider_received_amount}}</span></h5>
                <h5>Total Give Amount : <span>{{$provider_give_amount}}</span></h5>
                <h5>Wallet : <span>{{$wallet_provider_amount}}</span></h5>

                <table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
                    <thead>
                    <tr>
                        <th> Company Name</th>
                        <th> Amount</th>
                        <th> Order wise share</th>
                    </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Company</td>
                            <td>{{ ($company_share_amount - ($received_amount - $give_amount)) < 1 ? 0 : ($company_share_amount - ($received_amount - $give_amount)) }}</td>
                            <td><a href="{{route('staff.get.user.companyOrder',$staff_id)}}">View Order Wise</a></td>
                        </tr>
                    </tbody>
                </table>

            </div>
        </div>
    </div>
@endsection
@push('models')

    <div id="payment" class="modal fade mw-100 w-75" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Payment</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <form class="m-form" method="post" action="{{ route('staff.sendpayment') }}" id="create"
                          enctype="multipart/form-data" role="form">
                        @csrf
                        <input type="hidden" id="txtorderid" name="order_id">
                        <div class="m-portlet__body">
                            <div class="m-form__section m-form__section--first">

                                <div class="form-group row">

                                    <div class="col-md-12">
                                        <label for="total_amount"
                                               class="col-md-12 col-form-label text-md-left">{{ __('Total Amount') }}</label>
                                        <input id="total_amount" type="number"
                                               class="form-control @error('total_amount') is-invalid @enderror"
                                               name="total_amount"
                                               autocomplete="total_amount" autofocus min="1" required="">

                                        @error('total_amount')
                                        <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">

                                    <div class="col-md-12">
                                        <label for="payment_mode"
                                                   class="col-md-12 col-form-label text-md-left">{{ __('Payment Mode') }}
                                                <span class="mandatorySign">*</span></label>

                                            <select id="payment_mode"
                                                    class="form-control payment_mode @error('staff_id') is-invalid @enderror"
                                                    name="payment_mode" autocomplete="payment_mode" required="" onchange="forimagehide()">
                                                <option value="">Select a Payment Mode</option>
                                                <option value="Bank">Bank</option>
                                                <option value="EasyPaisa">EasyPaisa</option>
                                                <option value="Cash">Cash</option>
                                            </select>
                                            @error('payment_mode')
                                            <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                </div>
                                <div class="form-group row" id="imageshow">

                                    <div class="col-md-12">
                                        <label for="deposit_slip"
                                                   class="col-md-6 col-form-label text-md-left">{{ __('Deposit Slip') }}
                                                <span class="mandatorySign">*</span></label>
                                            <input value="{{old('deposit_slip')}}" type="file"
                                                   class="form-control @error('deposit_slip') is-invalid @enderror"
                                                   onchange="readURL(this)" id="deposit_slip"
                                                   name="deposit_slip" style="padding: 9px; cursor: pointer" required="">
                                            <img width="300" height="200" class="img-thumbnail" style="display:none;"
                                                 id="img" src="#"
                                                 alt="your image"/>

                                            @error('deposit_slip')
                                            <span class="invalid-feedback" role="alert">
                                              <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                    </div>
                                </div>


                            </div>
                        </div>
                        <div class="m-portlet__foot m-portlet__foot--fit text-md-right">
                            <div class="m-form__actions m-form__actions">
                                <a href="#" class="btn btn-info" data-dismiss="modal">Close</a>
                                <button type="submit" class="btn btn-primary">
                                    {{ __('SAVE') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>

            </div>

        </div>
    </div>
@endpush
@push('js')

    <script>


        var table = $("#m_table_1").DataTable({
            "order": [[0, "desc"]],
            "columnDefs": [
                {orderable: false, targets: [7]}
            ],
        });

        function forimagehide(){
            var value = $("#payment_mode option:selected").val();
            if(value == "Cash"){
                $("#imageshow").attr('hidden','hidden');
                $("#deposit_slip").removeAttr('required');
            }
            else{
             $("#imageshow").removeAttr('hidden');
             $("#deposit_slip").attr('required','required');
            }
        }

    </script>

@endpush
