@extends('service-provider.main')
@section('title','Ledger')
@push('css')
    <style>
        .map-image {
            height: 60px;
            width: 60px;
            border-radius: 50px !important;
            margin-right: 15px;
            float: left;
        }

        .mw-100 {
            margin: 0 auto;
        }

    </style>
@endpush
@section('content')
    <div class="m-content">
        <!--Begin::Section-->
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Ledger
                        </h3>
                    </div>
                </div>
            </div>

            <div class="m-portlet__body">
                {{--tableScroll--}}

                <table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
                    <thead>
                    <tr>
                        <th> Order No.</th>
                        <th> Customer Name</th>
                        <th> Service Provider Name</th>
                        <th> Completion Date/Time</th>
                        <th> Service Provider Comission %</th>
                        <th> Grand Total</th>
                        <th> Service Provider Share</th>
                        <th> Company Share</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(!empty($orders))
                        @foreach($orders as $key=>$order)
                            <tr>
                                <td><a href="{{route('staff.user.appointment.show',$order->id)}}">{{$order->order_id}}</a></td>
                                <td>{{$order->user->fullName()}}</td>
                                <td>{{$order->staff->fullName()}}</td>
                                <td>{{$order->order_end_time}}</td>
                                <td>{{$order->staff->comission->staff_comission}}</td>
                                <td>{{$order->total_price}}</td>
                                <td>{{($order->total_price*$order->staff->comission->staff_comission)/100  }}</td>
                                <td>{{($order->total_price*$order->staff->comission->company_comission)/100  }}</td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                    <tfoot>
                    <tr>
                        <th colspan="5" style="text-align:right"><b>Total:</b></th>
                        <th></th>
                        <th></th>
                        <th></th>

                    </tr>
                    </tfoot>
                </table>

            </div>
        </div>
    </div>
@endsection
@push('models')

    <div id="trackEmployee" class="modal fade mw-100 w-75" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Track Employee</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>

                </div>
                <div class="modal-body">
                    <div class="row">

                        <div class="col-lg-12 col-xs-12 col-sm-12">

                            <div id="address-map-container" style="width:100%;height:400px; ">
                                <div style="width: 100%; height: 100%" id="map"></div>
                            </div>

                        </div>


                    </div>
                </div>

            </div>

        </div>
    </div>
@endpush
@push('js')

    <script>


        var table = $("#m_table_1").DataTable({
            "footerCallback": function ( row, data, start, end, display ) {
                var api = this.api(), data;

                // Remove the formatting to get integer data for summation
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };

                totalprice = api
                    .column( 5 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                totalprovider = api
                    .column( 6 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                totalcomapny = api
                    .column( 7 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                // Update footer
                $( api.column( 5 ).footer() ).html(
                    totalprice);
                $( api.column( 6 ).footer() ).html(
                    totalprovider);
                $( api.column( 7 ).footer() ).html(
                    totalcomapny);
            },
            "order": [[0, "desc"]],
            "columnDefs": [
                {orderable: false, targets: [7]}
            ],
        });


        $('.order-status-change').change(function () {
            form = $(this).closest('form');
            node = $(this);
            var order_status = $(this).val();
            var order_id = $(this).data('order-id');
            var request = {"order_status": order_status, "order_id": order_id};
            if (order_status !== '') {
                $.ajax({
                    type: "GET",
                    url: "{{ route('ajax.order.status') }}",
                    data: request,
                    dataType: "json",
                    cache: true,
                    success: function (response) {
                        if (response.status == "success") {
                            toastr['success']("Order Status Change Successfully");
                        }
                    },
                    error: function () {
                        toastr['error']("Something Went Wrong.");
                    }
                });
            } else {
                toastr['error']("Please Select Order Status");
            }
        });

        function assingStaff(value) {

            var staff_id = $(value).val();
            var order_id = $(value).data('order-id');
            var request = {"order_id": order_id, "staff_id": staff_id};
            if (staff_id !== '') {
                $.ajax({
                    type: "GET",
                    url: "{{ route('ajax.assign.staff') }}",
                    data: request,
                    dataType: "json",
                    cache: true,
                    success: function (response) {
                        if (response.status == "success") {
                            toastr['success']("Staff Assigned Successfully");

                            setTimeout(function () {
                                window.location = '{{route('admin.get.order.history')}}';
                            }, 1500);
                        }
                    },
                    error: function () {
                        toastr['error']("Something Went Wrong.");
                    }
                });
            } else {
                toastr['error']("Please Select Staff");
            }
        }
    </script>


    <script
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAZQiMEU1xYMEVTgch8O5WmL-iZVfQjko0&libraries=places"
        async defer></script>

    <script type="text/javascript">
        var locations = [];

        function getLatLng(id) {
            ;
            var order_id = parseInt(id);
            var request = {"order_id": order_id};
            if (order_id !== '') {
                $.ajax({
                    type: "GET",
                    url: "{{ route('ajax.get.latlng') }}",
                    data: request,
                    dataType: "json",
                    cache: true,
                    success: function (response) {
                        if (response.status == "success") {


                            var staff_image = '/uploads/pointers/staff_pointer.png';
                            var customer_image = '/uploads/pointers/customer_pointer.png';
                            locations.push([response.data.staff_latlng.address, response.data.staff_latlng.latitude, response.data.staff_latlng.longitude, response.data.staff_latlng.first_name, response.data.staff_latlng.last_name, response.data.staff_latlng.profile_pic, staff_image]);
                            locations.push([response.data.customer_latlng.address, response.data.customer_latlng.latitude, response.data.customer_latlng.longitude, response.data.customer_latlng.first_name, response.data.customer_latlng.last_name, response.data.customer_latlng.profile_pic, customer_image]);
                            initialize();

                            $('#trackEmployee').modal('show');

                            {{--toastr['success']("Staff Assigned Successfully");--}}

                            {{--setTimeout(function () {--}}
                            {{--    window.location = '{{route('admin.get.order.history')}}';--}}
                            {{--}, 1500);--}}
                        }
                    },
                    error: function () {
                        toastr['error']("Something Went Wrong.");
                    }
                });
            } else {
                toastr['error']("Please Select Staff");
            }
        }

        function initialize() {


                {{--var staffs = @json($staff_users);--}}



                {{--$.each(staffs, function (index, object) {--}}
                {{--    locations.push([object.address, object.latitude, object.longitude, object.first_name, object.last_name, object.profile_pic])--}}
                {{--});--}}


            var map = new google.maps.Map(document.getElementById('map'), {
                    zoom: 12,
                    center: new google.maps.LatLng(31.5204, 74.3587),
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                });

            var infowindow = new google.maps.InfoWindow();

            var marker, i;

            for (i = 0; i < locations.length; i++) {
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                    map: map,
                    icon: locations[i][6]
                });

                google.maps.event.addListener(marker, 'click', (function (marker, i) {
                    return function () {
                        var contentString = '';

                        contentString += '<div id="content">';
                        contentString += '<div id="siteNotice">';
                        contentString += '<img id="image" name="image" src="/uploads/user_profiles/' + locations[i][5] + '"  class="map-image"/>';
                        contentString += '<h5 id="firstHeading" class="firstHeading">' + locations[i][3] + ' ' + locations[i][4] + '</h5>';//doesnt work here
                        contentString += '<div id="bodyContent">' + locations[i][0] + '</div>';
                        contentString += '</div>';

                        infowindow.setContent(contentString);
                        // infowindow.setContent(locations[i][0]);
                        infowindow.open(map, marker);
                    }
                })(marker, i));
            }
        }

    </script>

@endpush
