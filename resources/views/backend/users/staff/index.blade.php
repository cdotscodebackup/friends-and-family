@extends('layouts.master')
@section('title','Staff User')
@section('content')

    <div class="m-content">
        <!--Begin::Section-->
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Staffs
                        </h3>
                    </div>
                </div>

                <div class="m-portlet__head-tools">
                    <ul class="m-portlet__nav">
                        <li class="m-portlet__nav-item">
                            <a href="{{ route('admin.staffs.create') }}"
                               class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                                <span>
                                    <i class="la la-plus"></i>
                                    <span>Add Staff</span>
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="m-portlet__body">
                <input type="hidden">

                <table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
                    <thead>
                    <tr>

                        <th> Sr NO.</th>
                        <th> Full Name</th>
                        <th> Email</th>
                        <th> Comissions</th>
                        <th> Company Comission</th>
                        <th> Staff Comission</th>
                        <th> Status</th>
                        <th> Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(!empty($users))
                        @foreach($users as $user)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{ucfirst($user->fullname())}}</td>
                                <td>{{$user->email}}</td>
                                <td>
                                    @if($user->user_type == 'driver' || $user->user_type =='customer')
                                        --
                                    @else
                                        <select id="comission_id"
                                                class="form-control assign-comission @error('comission_id') is-invalid @enderror"
                                                name="comission_id" autocomplete="comission_id"
                                                data-user-id="{{$user->id}}">
                                            <option value="">Select an option</option>
                                            @if(!empty($comissions))
                                                @foreach($comissions as $comission)
                                                    <option
                                                        value="{{$comission->id}}" {{ ($user->comission_id == $comission->id) ? 'selected' : ''}}>{{ucfirst($comission->title)}}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    @endif

                                </td>


                                <td>{{($user->comission) ? $user->comission->company_comission ?? "--" : "--"}}</td>
                                <td>{{($user->comission) ? $user->comission->staff_comission   ?? "--" : "--"}}</td>

                                <td style="width: 10%;">{{ucfirst($user->status)}}</td>
                                <td nowrap style="width: 12%;">
                                    <a href="{{route('admin.staffs.edit',$user->id)}}"
                                       class="btn btn-sm btn-info pull-left ">Edit</a>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
@push('js')
    <script>
        $("#m_table_1").dataTable({
            // "ordering": false,
            "order": [[0, "asc"]],
            "columnDefs": [
                {orderable: false, targets: [7]}
            ],
        });

        $(document).on('change', '.assign-comission', function () {

            form = $(this).closest('form');
            node = $(this);
            var comission_id = $(this).val();
            var user_id = $(this).data('user-id');
            var request = {"comission_id": comission_id, "user_id": user_id};
            if (comission_id !== '') {
                $.ajax({
                    type: "GET",
                    url: "{{ route('ajax.assign.comission') }}",
                    data: request,
                    dataType: "json",
                    cache: true,
                    success: function (response) {
                        if (response.status == "success") {
                            toastr['success']("Comission Assign Successfully");
                            window.location.reload();
                        }
                    },
                    error: function () {
                        toastr['error']("Something Went Wrong.");
                    }
                });
            } else {
                toastr['error']("Please Select Membership");
            }

        });


    </script>
@endpush
