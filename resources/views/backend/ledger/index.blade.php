@extends('layouts.master')
@section('title','Ledger')
@push('css')
    <style>
        .map-image {
            height: 60px;
            width: 60px;
            border-radius: 50px !important;
            margin-right: 15px;
            float: left;
        }

        .mw-100 {
            margin: 0 auto;
        }

    </style>
@endpush
@section('content')
    <div class="m-content">
        <!--Begin::Section-->
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Ledger
                        </h3>
                    </div>
                </div>
            </div>

            <div class="m-portlet__body">
                {{--tableScroll--}}

                <table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
                    <thead>
                    <tr>
                        <th> Order No.</th>
                        <th> Customer Name</th>
                        <th> Service Provider Name</th>
                        <th> Completion Date/Time</th>
                        <th> Company Comission %</th>
                        <th> Grand Total</th>
                        <th> Service Provider Share</th>
                        <th> Company Share</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(!empty($orders))
                        @foreach($orders as $key=>$order)
                            <tr>
                                <td><a href="{{route('admin.get.user.order.show',$order->id)}}">{{$order->order_id}}</a></td>
                                <td>{{$order->user->fullName()}}</td>
                                <td>{{$order->staff->fullName()}}</td>
                                <td>{{$order->order_end_time}}</td>
                                <td>{{$order->staff->comission->company_comission}}</td>
                                <td>{{$order->total_price}}</td>
                                <td>{{($order->total_price*$order->staff->comission->staff_comission)/100  }}</td>
                                <td>{{($order->total_price*$order->staff->comission->company_comission)/100  }}</td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                    <tfoot>
                    <tr>
                        <th colspan="5" style="text-align:right"><b>Total:</b></th>
                        <th></th>
                        <th></th>
                        <th></th>
                    </tr>
                    </tfoot>
                </table>

            </div>
        </div>
    </div>
@endsection

@push('js')

    <script>


        var table = $("#m_table_1").DataTable({
            "footerCallback": function (row, data, start, end, display) {
                var api = this.api(), data;

                // Remove the formatting to get integer data for summation
                var intVal = function (i) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '') * 1 :
                        typeof i === 'number' ?
                            i : 0;
                };

                totalprice = api
                    .column(5)
                    .data()
                    .reduce(function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);

                totalprovider = api
                    .column(6)
                    .data()
                    .reduce(function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);

                totalcomapny = api
                    .column(7)
                    .data()
                    .reduce(function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0);

                // Update footer
                $(api.column(5).footer()).html(
                    totalprice);
                $(api.column(6).footer()).html(
                    totalprovider);
                $(api.column(7).footer()).html(
                    totalcomapny);
            },
            "order": [[0, "desc"]],
            "columnDefs": [
                {orderable: false, targets: [7]}
            ],
        });

    </script>

@endpush
