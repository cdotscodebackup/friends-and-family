@extends('layouts.master')
@section('title','Comissions')
@section('content')


    <div class="m-content">
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Add {{ __('Comission') }}
                        </h3>
                    </div>
                </div>
            </div>

            <div class="m-portlet__body">
                <div class="col-lg-12">
                    <div class="m-portlet">
                        <form class="m-form" method="post" action="{{ route('admin.comissions.store') }}" id="create"
                              enctype="multipart/form-data" role="form">
                            @csrf
                            <div class="m-portlet__body">
                                <div class="m-form__section m-form__section--first">

                                    <div class="form-group row">

                                        <div class="col-md-4">
                                            <label for="title"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('Title') }} <span
                                                    class="mandatorySign">*</span></label>
                                            <input id="title" type="text"
                                                   class="form-control @error('title') is-invalid @enderror"
                                                   name="title" value="{{ old('title') }}"
                                                   autocomplete="title" autofocus>

                                            @error('title')
                                            <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-4">
                                            <label for="company_comission"
                                                   class="col-md-8 col-form-label text-md-left">{{ __('Company Comission (%)') }}
                                                <span class="mandatorySign">*</span></label>
                                            <input id="company_comission" type="number"
                                                   class="form-control @error('company_comission') is-invalid @enderror"
                                                   name="company_comission" value="0"
                                                   min="1" max="100"
                                                   autocomplete="company_comission" autofocus>

                                            @error('company_comission')
                                            <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-4">
                                            <label for="staff_comission"
                                                   class="col-md-8 col-form-label text-md-left">{{ __('Staff Comission (%)') }}
                                                <span class="mandatorySign">*</span></label>
                                            <input id="staff_comission" type="number"
                                                   class="form-control @error('staff_comission') is-invalid @enderror"
                                                   name="staff_comission" value="" min="1"
                                                   max="100" readonly
                                                   autocomplete="staff_comission" autofocus>

                                            @error('staff_comission')
                                            <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                            @enderror
                                        </div>

                                    </div>


                                </div>
                            </div>
                            <div class="m-portlet__foot m-portlet__foot--fit text-md-right">
                                <div class="m-form__actions m-form__actions">
                                    <a href="{{ route('admin.comissions.index') }}" class="btn btn-info">Back</a>
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('SAVE') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                        <!--end::Form-->
                    </div>
                    <!--end::Portlet-->
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script>
        $(function () {


            $(document).on('keyup', 'input[name=company_comission]', function () {
                var _this = $(this);
                var staff_commission = 0;
                var min = parseInt(_this.attr('min')) || 0; // if min attribute is not defined, 1 is default
                var max = parseInt(_this.attr('max')) || 100; // if max attribute is not defined, 100 is default
                var val = parseInt(_this.val()) //|| (min - 1); // if input char is not a number the value will be (min - 1) so first condition will be true
                if (val < min) {
                    _this.val(min);
                    staff_commission = 100 - parseInt(min);
                    // $('#staff_comission').val(staff_commission);
                }

                if (val > max) {
                    _this.val(max);
                    staff_commission = 100 - parseInt(max);
                    $('#staff_comission').val(staff_commission);
                }

            });

            $('#company_comission').change(function (){
                $('#staff_comission').val(100 - parseInt($(this).val()));
            })

        });
    </script>
@endpush
