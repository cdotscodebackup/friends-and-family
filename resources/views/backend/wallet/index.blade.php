@extends('layouts.master')
@section('title','Wallet')
@push('css')
    <style>
        .map-image {
            height: 60px;
            width: 60px;
            border-radius: 50px !important;
            margin-right: 15px;
            float: left;
        }

        .mw-100 {
            margin: 0 auto;
        }

    </style>
@endpush
@section('content')
    <div class="m-content">
        <!--Begin::Section-->
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Wallet
                        </h3>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">
                {{--tableScroll--}}
                <h5>Total Recived Against Orders : <span>{{$recived_amount}}</span></h5>
                <h5>Total Recived Against UnCompleted Orders : <span>{{$total_uncompleted_order_amount}}</span></h5>
                <h5>Total Paid Against Orders : <span>{{$give_amount}}</span></h5>
                <h5>Company Account : <span>{{$wallet_amount}}</span></h5>

                <table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
                    <thead>
                    <tr>
                        <th> Service Provider Name</th>
                        <th> Amount</th>
                        <th> Order wise share</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(!empty($staffs))
                        @foreach($staffs as $key=>$staff)
                            <tr>
                                <td>{{$staff->fullName()}}</td>
                                <td>{{ ($staff->order - ($staff->provider_received_amount - $staff->provider_give_amount))  }}</td>
                                <td><a href="{{route('admin.get.user.staffOrder', $staff->id)}}">View Order Wise</a></td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>

            </div>
        </div>
    </div>
@endsection
@push('js')

    <script>


        var table = $("#m_table_1").DataTable({
            "order": [[0, "desc"]],
            "columnDefs": [
                {orderable: false, targets: [7]}
            ],
        });

    </script>

@endpush
