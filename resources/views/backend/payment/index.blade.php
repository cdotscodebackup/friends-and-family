@extends('layouts.master')
@section('title','Providers Sent Payment')
@push('css')
    <style>
        .map-image {
            height: 60px;
            width: 60px;
            border-radius: 50px !important;
            margin-right: 15px;
            float: left;
        }

        .mw-100 {
            margin: 0 auto;
        }

        .mr-10 {
            margin-right: 10px;
        }

        .mb-15 {
            margin-bottom: 15px;
        }

    </style>
@endpush
@section('content')
    <div class="m-content">
        <!--Begin::Section-->
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Providers sent Payment
                        </h3>
                    </div>
                </div>
                <div class="m-portlet__head-tools">
                    <ul class="m-portlet__nav">
                        <li class="m-portlet__nav-item">
                            <a data-target="#payment" data-toggle="modal"
                               class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                                <span>
                                    <i class="la la-plus"></i>
                                    <span>Payment</span>
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="m-portlet__body">
                {{--tableScroll--}}

                <ul class="nav nav-pills">
                    <li class="active btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air mr-10">
                        <a href="#tab_2_1" data-toggle="tab" aria-expanded="true" class="text-white">
                            Unverfied Payment  </a>
                    </li>
                    <li class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air mr-10">
                        <a href="#tab_2_3" data-toggle="tab" aria-expanded="false" class="text-white">
                            Verfied Payment </a>
                    </li>

                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_2_1">
                        <table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
                            <thead>
                            <tr>
                                <th> Service Provider Name</th>
                                <th> Recived Amount Date</th>
                                <th> Payment Mode</th>
                                <th> Amount</th>
                                <th> Deposit Slip</th>
                                <th> Status</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(!empty($provider_send_payments))
                                @foreach($provider_send_payments as $key=>$provider_send_payment)
                                    <tr>
                                        <td>{{$provider_send_payment->staff->fullName()}}</td>
                                        <td>{{$provider_send_payment->created_at}}</td>
                                        <td>{{$provider_send_payment->payment_mode}}</td>
                                        <td>{{$provider_send_payment->total_amount}}</td>
                                        <td>
                                            @if($provider_send_payment->payment_mode == "Cash")
                                            <button type="button" class="btn btn-sm btn-accent pull-left" onclick="ProviderPaymentVerifyoncash('{{$provider_send_payment->id}}')">Vreifiy</button>
                                            @else
                                                <button type="button" class="btn btn-sm btn-accent pull-left" onclick="payment_image_view('{{$provider_send_payment->deposit_slip_path}}','{{$provider_send_payment->id}}')">Image</button>
                                            @endif
                                        </td>
                                        <td>{{$provider_send_payment->status}}</td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                    <div class="tab-pane" id="tab_2_3">
                        <table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_2">
                            <thead>
                            <tr>
                                <th> Service Provider Name</th>
                                <th> Recived Amount Date</th>
                                <th> Payment Mode</th>
                                <th> Amount</th>
                                <th> Deposit Slip</th>
                                <th> Status</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if(!empty($provider_send_payments_verifies))
                                @foreach($provider_send_payments_verifies as $key=>$provider_send_payments_verify)
                                    <tr>
                                        <td>{{$provider_send_payments_verify->staff->fullName()}}</td>
                                        <td>{{$provider_send_payments_verify->created_at}}</td>
                                        <td>{{$provider_send_payments_verify->payment_mode}}</td>
                                        <td>{{$provider_send_payments_verify->total_amount}}</td>
                                        <td>
                                            @if($provider_send_payments_verify->payment_mode == "Cash")
                                            @else
                                                <button type="button" class="btn btn-sm btn-accent pull-left" onclick="verified_payment_image_view('{{$provider_send_payments_verify->deposit_slip_path}}')">Image</button>
                                            @endif
                                        </td>
                                        <td>{{$provider_send_payments_verify->status}}</td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>



            </div>

        </div>
    </div>
@endsection
@push('models')


    <div id="payment_image" class="modal fade mw-100 w-75" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Deposit Slip Image</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <form class="m-form"  role="form">
                        <input type="hidden" id="txtslipid">
                        <div class="m-portlet__body">
                            <div class="m-form__section m-form__section--first">

                                <div class="form-group row">

                                    <div class="col-md-12">
                                            <img width="300" height="200" class="img-thumbnail"
                                                 id="img" src="#"
                                                 alt="your image"/>
                                    </div>
                                </div>


                            </div>
                        </div>
                        <div class="m-portlet__foot m-portlet__foot--fit text-md-right">
                            <div class="m-form__actions m-form__actions">
                                <a href="#" class="btn btn-info" data-dismiss="modal">Reject</a>
                                <button type="button" class="btn btn-primary" onclick="ProviderPaymentVerify()">verify</button>
                            </div>
                        </div>
                    </form>
                </div>

            </div>

        </div>
    </div>
    <div id="verified_payment_image" class="modal fade mw-100 w-75" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Deposit Slip Image</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <form class="m-form"  role="form">
                        <div class="m-portlet__body">
                            <div class="m-form__section m-form__section--first">

                                <div class="form-group row">

                                    <div class="col-md-12">
                                            <img width="300" height="200" class="img-thumbnail"
                                                 id="verified_image" src="#"
                                                 alt="your image"/>
                                    </div>
                                </div>


                            </div>
                        </div>
                        <div class="m-portlet__foot m-portlet__foot--fit text-md-right">
                            <div class="m-form__actions m-form__actions">
                                <a href="#" class="btn btn-info" data-dismiss="modal">Close</a>
                            </div>
                        </div>
                    </form>
                </div>

            </div>

        </div>
    </div>

    <div id="payment" class="modal fade mw-100 w-75" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Payment</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <form class="m-form" method="post" action="{{ route('admin.payment.store') }}" id="create"
                          enctype="multipart/form-data" role="form">
                        @csrf
                        <div class="m-portlet__body">
                            <div class="m-form__section m-form__section--first">

                                <div class="form-group row">

                                    <div class="col-md-12">
                                        <label for="staff_id"
                                                   class="col-md-12 col-form-label text-md-left">{{ __('Staff') }}
                                                <span class="mandatorySign">*</span></label>

                                            <select id="staff_id"
                                                    class="form-control categories @error('staff_id') is-invalid @enderror"
                                                    name="staff_id" autocomplete="staff_id" required="">
                                                <option value="">Select a Staff</option>
                                                @if(!empty($staffs))
                                                    @foreach($staffs as $staff)
                                                        <option value="{{ $staff->id }}">{{ucfirst($staff->fullName())}}</option>
                                                    @endforeach
                                                @endif

                                            </select>
                                            @error('staff_id')
                                            <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                </div>

                                <div class="form-group row">

                                    <div class="col-md-12">
                                        <label for="payment_mode"
                                                   class="col-md-12 col-form-label text-md-left">{{ __('Payment Mode') }}
                                                <span class="mandatorySign">*</span></label>

                                            <select id="payment_mode"
                                                    class="form-control payment_mode @error('staff_id') is-invalid @enderror"
                                                    name="payment_mode" autocomplete="payment_mode" required="" onchange="forimagehide()">
                                                <option value="">Select a Payment Mode</option>
                                                <option value="Bank">Bank</option>
                                                <option value="EasyPaisa">EasyPaisa</option>
                                                <option value="Cash">Cash</option>
                                            </select>
                                            @error('payment_mode')
                                            <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                </div>

                                <div class="form-group row">

                                    <div class="col-md-12">
                                        <label for="total_amount"
                                               class="col-md-12 col-form-label text-md-left">{{ __('Total Amount') }}</label>
                                        <input id="total_amount" type="number"
                                               class="form-control @error('total_amount') is-invalid @enderror"
                                               name="total_amount"
                                               autocomplete="total_amount" autofocus min="1" required="">

                                        @error('total_amount')
                                        <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group row" id="imageshow">

                                    <div class="col-md-12">
                                        <label for="deposit_slip"
                                                   class="col-md-6 col-form-label text-md-left">{{ __('Deposit Slip') }}
                                                <span class="mandatorySign">*</span></label>
                                            <input value="{{old('deposit_slip')}}" type="file"
                                                   class="form-control @error('deposit_slip') is-invalid @enderror"
                                                   onchange="readURL(this)" id="deposit_slip"
                                                   name="deposit_slip" style="padding: 9px; cursor: pointer" required="">
                                            <img width="300" height="200" class="img-thumbnail" style="display:none;"
                                                 id="img" src="#"
                                                 alt="your image"/>

                                            @error('deposit_slip')
                                            <span class="invalid-feedback" role="alert">
                                              <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                    </div>
                                </div>


                            </div>
                        </div>
                        <div class="m-portlet__foot m-portlet__foot--fit text-md-right">
                            <div class="m-form__actions m-form__actions">
                                <a href="#" class="btn btn-info" data-dismiss="modal">Close</a>
                                <button type="submit" class="btn btn-primary">
                                    {{ __('SAVE') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>

            </div>

        </div>
    </div>
@endpush
@push('js')

    <script>


        var table = $("#m_table_1").DataTable({
            "order": [[0, "desc"]],

        });

        var table2 = $("#m_table_2").DataTable({
            "order": [[0, "desc"]],

        });
        function verified_payment_image_view(path){
            $("#verified_image").attr('src',path);

            $("#verified_payment_image").modal('show');
        }
        function  payment_image_view(path,id){
            $("#txtslipid").val(id);
            $("#img").attr('src',path);

            $("#payment_image").modal('show');
        }
        function ProviderPaymentVerify() {
            $.ajax({
                type: "POST",
                url: "{{ route('admin.providerpaymentverify') }}",
                data: {"_token": "{{ csrf_token() }}",id:$("#txtslipid").val()},
                dataType: "json",
                cache: true,
                success: function (response) {
                    if (response.status == true) {
                        toastr['success'](""+response.message+"");

                        setTimeout(function () {
                            window.location = '{{route('admin.payment.index')}}';
                        }, 1500);
                    }
                    else{
                        toastr['success'](""+response.message+"");
                    }
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });

        }

        function ProviderPaymentVerifyoncash(slipid) {
            $.ajax({
                type: "POST",
                url: "{{ route('admin.providerpaymentverify') }}",
                data: {"_token": "{{ csrf_token() }}",id:slipid },
                dataType: "json",
                cache: true,
                success: function (response) {
                    if (response.status == true) {
                        toastr['success'](""+response.message+"");

                        setTimeout(function () {
                            window.location = '{{route('admin.payment.index')}}';
                        }, 1500);
                    }
                    else{
                        toastr['success'](""+response.message+"");
                    }
                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });

        }

        function forimagehide(){
            var value = $("#payment_mode option:selected").val();
            if(value == "Cash"){
                $("#imageshow").attr('hidden','hidden');
                $("#deposit_slip").removeAttr('required');
            }
            else{
             $("#imageshow").removeAttr('hidden');
             $("#deposit_slip").attr('required','required');
            }
        }

    </script>

@endpush
