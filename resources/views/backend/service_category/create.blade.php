@extends('layouts.master')
@section('title','Services')
@section('content')

    <div class="m-subheader ">
        <div class="d-flex align-items-center">
            <div class="mr-auto">
                <h3 class="m-subheader__title ">{{ __('Services') }}</h3>
            </div>
        </div>
    </div>
    <div class="m-content">
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Add {{ __('Services') }}
                        </h3>
                    </div>
                </div>
            </div>

            <div class="m-portlet__body">
                <div class="col-lg-12">
                    <div class="m-portlet">
                        <form class="m-form" method="post" action="{{ route('admin.services.store') }}"
                              enctype="multipart/form-data" role="form">
                            @csrf
                            <div class="m-portlet__body">
                                <div class="m-form__section m-form__section--first">


                                    <div class="form-group row">


                                        <div class="col-md-6">
                                            <label for="name"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('Name') }} <span
                                                    class="mandatorySign">*</span></label>
                                            <input id="name" type="text"
                                                   class="form-control @error('name') is-invalid @enderror"
                                                   name="name" value="{{ old('name') }}"
                                                   autocomplete="name" autofocus>

                                            @error('name')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>

                                        <div class="col-md-6">
                                            <label for="slug"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('Slug') }} <span
                                                    class="mandatorySign">*</span></label>
                                            <input id="slug" type="text"
                                                   class="form-control @error('slug') is-invalid @enderror"
                                                   name="slug" value="{{ old('slug') }}"
                                                   autocomplete="slug" autofocus>

                                            @error('slug')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>


                                    <div class="form-group row">

                                        <div class="col-md-6">
                                            <label for="category_id"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('Categories') }}
                                                <span class="mandatorySign">*</span></label>

                                            <select id="category_id.0"
                                                    class="form-control categories @error('category_id.0') is-invalid @enderror"
                                                    name="category_id[]" autocomplete="category_id.0" multiple>
                                                <option value="">Select an option</option>
                                                @if(!empty($categories))
                                                    @foreach($categories as $category)
                                                        <option
                                                            value="{{$category->id}}">{{ucfirst($category->name)}}</option>
                                                    @endforeach
                                                @endif
                                            </select>

                                            @error('category_id.0')
                                            <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
{{--                                        <div class="col-md-4">--}}
{{--                                            <label for="price"--}}
{{--                                                   class="col-md-4 col-form-label text-md-left">{{ __('Price') }}<span--}}
{{--                                                    class="mandatorySign">*</span></label>--}}

{{--                                            <input id="price" type="number"--}}
{{--                                                   class="form-control @error('price') is-invalid @enderror"--}}
{{--                                                   name="price" value="{{ old('price') }}"--}}
{{--                                                   min="0" onkeypress="return event.charCode >= 48"--}}
{{--                                                   autocomplete="price" autofocus>--}}

{{--                                            @error('price')--}}
{{--                                            <span class="invalid-feedback" role="alert">--}}
{{--                                                 <strong>{{ $message }}</strong>--}}
{{--                                            </span>--}}
{{--                                            @enderror--}}
{{--                                        </div>--}}
                                        <div class="col-md-6">
                                            <label for="is_available"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('Status') }}</label>

                                            <select id="is_available"
                                                    class="form-control @error('is_available') is-invalid @enderror"
                                                    name="is_available" autocomplete="is_available">
                                                <option value="0">Non-Active</option>
                                                <option value="1" selected>Active</option>
                                            </select>

                                            @error('is_available')
                                            <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>

                                    </div>
                                    <div class="form-group row">

                                        <div class="col-md-12">
                                            <label for="summary"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('Summary') }}
                                                <span class="mandatorySign">*</span></label>

                                            <textarea id="summary"
                                                      class="form-control @error('summary') is-invalid @enderror"
                                                      name="summary" rows="3" cols="15"
                                                      autocomplete="summary">{{ old('summary') }}</textarea>

                                            @error('summary')
                                            <span class="invalid-feedback" role="alert">
                                              <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>


                                    </div>
                                    <div class="form-group row">

                                        <div class="col-md-12">
                                            <label for="description"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('Description') }}
                                                <span class="mandatorySign">*</span></label>

                                            <textarea id="description"
                                                      class="form-control @error('description') is-invalid @enderror"
                                                      name="description" rows="5" cols="15"
                                                      value="{{ old('description') }}"
                                                      autocomplete="description"></textarea>

                                            @error('description')
                                            <span class="invalid-feedback" role="alert">
                                              <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>


                                    </div>
                                    <div class="form-group row">

                                        <div class="col-md-4">
                                            <label for="image"
                                                   class="col-md-6 col-form-label text-md-left">{{ __('Banner Image (1170 * 390)') }}
                                                <span class="mandatorySign">*</span></label>
                                            <input value="{{old('image')}}" type="file"
                                                   class="form-control @error('image') is-invalid @enderror"
                                                   onchange="readURL(this)" id="image"
                                                   name="image" style="padding: 9px; cursor: pointer">
                                            <img width="300" height="200" class="img-thumbnail" style="display:none;"
                                                 id="img" src="#"
                                                 alt="your image"/>

                                            @error('image')
                                            <span class="invalid-feedback" role="alert">
                                              <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-4">
                                            <label for="thumbnail_image" id="image_thumb"
                                                   class="col-md-6 col-form-label text-md-left">{{ __('Thumbnail Image (350 * 270)') }}
                                                <span class="mandatorySign">*</span></label>
                                            <input value="{{old('thumbnail_image')}}" type="file" id="thumbnail_image"
                                                   class="form-control @error('thumbnail_image') is-invalid @enderror"
                                                   onchange="readURLThumbnail(this)"
                                                   name="thumbnail_image" style="padding: 9px; cursor: pointer">
                                            <img width="300" height="200" class="img-thumbnail" style="display:none;"
                                                 id="img_thumbnail" src="#"
                                                 alt="your image"/>

                                            @error('thumbnail_image')
                                            <span class="invalid-feedback" role="alert">
                                              <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>

                                        <div class="col-md-4">
                                            <label for="icon" id="image_icon"
                                                   class="col-md-6 col-form-label text-md-left">{{ __('Icon Image (350 * 270)') }}
                                                <span class="mandatorySign">*</span></label>
                                            <input value="{{old('icon')}}" type="file" id="icon_image"
                                                   class="form-control @error('icon') is-invalid @enderror"
                                                   onchange="readURLIcon(this)"
                                                   name="icon" style="padding: 9px; cursor: pointer">
                                            <img width="300" height="200" class="img-icon" style="display:none;"
                                                 id="img-icon" src="#"
                                                 alt="your image"/>

                                            @error('icon')
                                            <span class="invalid-feedback" role="alert">
                                              <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>


                                </div>

                                <div class="form-group row">
                                    <div class="col-md-6">
                                        <label for="meta_title"
                                               class="col-md-4 col-form-label text-md-left">{{ __('Meta Title') }} </label>

                                        <input id="meta_title" type="text"
                                               class="form-control @error('meta_title') is-invalid @enderror"
                                               name="meta_title"
                                               value="{{ old('meta_title') }}" autocomplete="meta_title">

                                        @error('meta_title')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                    <div class="col-md-6">
                                        <label for="meta_keywords"
                                               class="col-md-4 col-form-label text-md-left">{{ __('Meta Keywords') }}</label>

                                        <input id="meta_keywords" type="text"
                                               class="form-control @error('meta_keywords') is-invalid @enderror"
                                               name="meta_keywords"
                                               value="{{ old('meta_keywords') }}" autocomplete="meta_keywords">

                                        @error('meta_keywords')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="form-group row">

                                    <div class="col-md-12">
                                        <label for="meta_description"
                                               class="col-md-4 col-form-label text-md-left">{{ __('Meta Description') }}</label>

                                        <textarea id="meta_description"
                                                  class="form-control @error('meta_description') is-invalid @enderror"
                                                  name="meta_description"
                                                  value="{{ old('meta_description') }}"
                                                  autocomplete="meta_description"></textarea>

                                        @error('meta_description')
                                        <span class="invalid-feedback" role="alert">
                                              <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>

                                </div>

                            </div>
                            <div class="m-portlet__foot m-portlet__foot--fit text-md-right">
                                <div class="m-form__actions m-form__actions">
                                    <a href="{{ route('admin.services.index') }}" class="btn btn-info">Back</a>
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('SAVE') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                        <!--end::Form-->
                    </div>
                    <!--end::Portlet-->
                </div>
            </div>
        </div>
    </div>
@endsection


@push('js')
    <script>
        $('.categories').select2();
        // $('#out_house_appointment').click(function () {
        //     if ($(this).is(":checked")) {
        //         $(this).val(1);
        //     } else {
        //         $(this).val(0);
        //     }
        // });
        //
        // $('#in_house_appointment').click(function () {
        //     if ($(this).is(":checked")) {
        //         $(this).val(1);
        //     } else {
        //         $(this).val(0);
        //     }
        // });
    </script>
@endpush


@push('js')
    <script>
        $('#name').focusout(function () {

            var name = $(this).val();
            name = name.replace(/\s+/g, '-').toLowerCase();

            $('#slug').val(name);
        })
    </script>
@endpush
