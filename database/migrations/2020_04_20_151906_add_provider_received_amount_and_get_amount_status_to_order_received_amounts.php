<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddProviderReceivedAmountAndGetAmountStatusToOrderReceivedAmounts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_received_amounts', function (Blueprint $table) {
            $table->decimal('provider_received_amount')->default(0)->after('give_amount');
            $table->decimal('provider_give_amount')->default(0)->after('provider_received_amount');
            $table->string('description')->after('provider_give_amount');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_received_amounts', function (Blueprint $table) {
            //
        });
    }
}
