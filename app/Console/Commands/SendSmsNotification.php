<?php

namespace App\Console\Commands;

use App\Events\SendReferralCodeWithPhone;
use App\Models\Order;
use App\Models\User;
use DateTime;
use Illuminate\Console\Command;

class SendSmsNotification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sms:notification';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send SMS to staff and customer 2 hours before Appointment';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Fetching Records.....');
        $date = \Carbon\Carbon::now();

        Order::whereDate('created_at', $date)->where('order_status', 'confirmed')->where('staff_status', 'accepted')->where('order_progress_status', '=', null)->chunk(100, function ($orders) use ($date) {

            $orders->each(function ($order) use ($date) {
                foreach ($order->order_details as $orderDetail) {

                    if (is_null($orderDetail->deleted_at)) {
                        $order_requested_time = new DateTime($orderDetail->time);
                        $current_time = new DateTime($date);
                        $interval = $order_requested_time->diff($current_time);
                        $hours = $interval->h;//now do whatever you like with $days

                        if ($hours <= 2) {


                            $remove_zero = ltrim($order->user->phone_number, '0');
                            $add_number = '92';
                            $phone_number = $add_number . $remove_zero;
                            $message = 'Thank you for booking an appointment with Friends & Family. Someone from our team will come in your place and provide service.Your Order ID is::' . $order->order_id . '';
//                            event(new SendReferralCodeWithPhone($user_name = '', $phone_number, $message));


                            $remove_zero_from_staff = ltrim($order->staff->phone_number, '0');
                            $add_number_staff = '92';
                            $phone_number_staff = $add_number_staff . $remove_zero_from_staff;
                            $message_staff = 'Hello Service Provider you have scheduled an order please the order id and get ready for providing the service t0 client.Your Order ID is::' . $order->order_id . '';
//                            event(new SendReferralCodeWithPhone($user_name = '', $phone_number_staff, $message_staff));

                        }
                    }


                }
            });
        });

        $this->info('Command Run Successfully.....');
    }
}
