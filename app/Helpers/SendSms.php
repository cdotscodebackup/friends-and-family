<?php
/**
 * Created by PhpStorm.
 * User: Adil Mughal
 * Date: 12/24/2019
 * Time: 6:34 PM
 */

namespace App\Helpers;

use SoapClient;

Abstract Class SendSms
{
    public function sendSMS($phone_no, $message)
    {
        try {
            ini_set('soap.wsdl_cache_enabled', 0);
            ini_set('soap.wsdl_cache_ttl', 0);

            $opts = array(
                'ssl' => array(
                    'verify_peer' => false,
                    'verify_peer_name' => false,
                    'ciphers' => 'RC4-SHA',
                    'allow_self_signed' => true
                ),
                'http' => array(
                    'user_agent' => 'PHPSoapClient'
                ),
                'https' => array(
                    'user_agent' => 'PHPSoapClient'
                ),
            );
            $context = stream_context_create($opts);

            $wsdlUrl = 'http://cbs.zong.com.pk/reachcwsv2/corporatesms.svc?wsdl';

            $soapClientOptions = array(
                'trace' => 1,
                'exceptions' => 1,
                'stream_context' => $context,
                'cache_wsdl' => WSDL_CACHE_NONE,
                'encoding' => 'UTF-8',
                'verify_peer' => false,
                'verifyhost' => false,
            );
            $credentials = array(
                'loginId' => '923124742135',
                'loginPassword' => 'Zong@123'
            );

            libxml_disable_entity_loader(false); //adding this worked for me
            $client = new SoapClient($wsdlUrl, $soapClientOptions);
            $smsParameters = array(
                'obj_QuickSMS' => array(
                    'loginId' => '923124742135',
                    'loginPassword' => 'Zong@123',
                    'Destination' => $phone_no,
                    'Mask' => 'Instalment',
                    'Message' => $message,
                    'UniCode' => '0',
                    'ShortCodePrefered' => 'n',
                )
            );


            $result = $client->QuickSMS($smsParameters);

//            echo '<pre>';
//            print_r($result);
//            echo '</pre>';

            return true;
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
}
