<?php

namespace App\Http\Controllers\Backend;


use App\Http\Controllers\Controller;
use App\Models\ProviderSendPayment;
use App\Models\User;
use Illuminate\Http\Request;


class PaymentController extends Controller
{

    public function index()
    {
        $provider_send_payments = ProviderSendPayment::with('staff')->where('status','unverify')->get();
        $provider_send_payments_verifies = ProviderSendPayment::with('staff')->where('status','verified')->get();
        $staffs = User::where('user_type','staff')->get();
        return view('backend.payment.index', compact('provider_send_payments','provider_send_payments_verifies','staffs'));
    }

    public function store(Request $request){
        if($request->total_amount  == 0 ){
            return redirect()->route('admin.payment.index')
            ->with([
                'flash_status' => 'error',
                'flash_message' => 'Please enter amount and deposit slip.'
            ]);
        }
        else{
            $deposit_slip_path = null;
            if ($request->has('deposit_slip')) {
                $image = $request->file('deposit_slip');
                $name = $image->getClientOriginalName();
                $name = str_replace(" ","",$name);
                $destinationPath = public_path('/uploads/deposit_slips');
                $imagePath = $destinationPath . "/" . $name;
                $image->move($destinationPath, $name);
                $deposit_slip_path ='https://familyfriends.pk/uploads/deposit_slips/' . $name;
    
    
            } else {
                $deposit_slip_path = null;
            }

        $provider_send_payment                      = new ProviderSendPayment();
        $provider_send_payment->staff_id            = $request->staff_id;
        $provider_send_payment->total_amount        = (int)$request->total_amount;
        $provider_send_payment->deposit_slip_path   = $deposit_slip_path;
        $provider_send_payment->payment_mode        = $request->payment_mode;
        $provider_send_payment->save();
        return redirect()->route('admin.payment.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Payment add successfully.'
            ]);
        }
    }
}
