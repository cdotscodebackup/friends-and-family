<?php

namespace App\Http\Controllers\Backend;
use App\Models\Banner;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BannerController extends Controller
{

    public function __construct()
    {
        $this->middleware('role:admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $banners = Banner::orderBy('id', 'DESC')->get();
        return view('backend.Banners.index', compact('banners'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.Banners.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
            'image' => 'mimes:jpeg,jpg,png|required|max:2048',
            'url' => 'required|string',
        ], [
            'title.required' => 'Title is required',
            'description.required' => 'Description is required',
            'image.required' => 'Please Select valid image ',
            'url.required' => 'Url not valid',
        ]);


        $Banners = Banner::all();

        
        if (count($Banners) > 3) {
            return redirect()->route('admin.banner.index')
                ->with([
                    'flash_status' => 'error',
                    'flash_message' => 'You can only add 3 Banners'
                ]);
        } else {

            $image = $request->file('image');
            $name = $image->getClientOriginalName();
            $destinationPath = public_path('/uploads/banner');
            $image->move($destinationPath, $name);
            $image_name = $name;
            Banner::create([
                'title' => $request->title,
                'description' => $request->description,
                'image' => $image_name,
                'url' => $request->url,
            ]);

            return redirect()->route('admin.banner.index')
                ->with([
                    'flash_status' => 'success',
                    'flash_message' => 'Banner created successfully.'
                ]);
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $banner = Banner::whereId($id)->first();
        return view('backend.Banners.edit', compact('banner'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
            'image' => 'mimes:jpeg,jpg,png|required|max:2048',
            'url' => 'required|string',
        ], [
            'title.required' => 'Title is required',
            'description.required' => 'Description is required',
            'image.required' => 'Please Select valid image ',
            'url.required' => 'Url not valid',
        ]);

        $other = Banner::whereId($id)->first();

        if($request->image){
            $image_path = public_path('uploads/banner/').$other->image;
        
            if(file_exists($image_path)){
                unlink($image_path);
            }
            $image = $request->file('image');
            $name = $image->getClientOriginalName();
            $destinationPath = public_path('/uploads/banner');
            $image->move($destinationPath, $name);
            $image_name = $name;
        }

        $other->fill([
            'title' => $request->title,
            'description' => $request->description,
            'image' => $image_name,
            'url' => $request->url,
        ]);
        $other->save();
        return redirect()->route('admin.banner.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Banner updated successfully.'
            ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $Banner = Banner::findOrFail($id);
        $image_path = public_path('uploads/banner/').$Banner->image;
        
        if(file_exists($image_path)){
            unlink($image_path);
        }

        $Banner->delete();
        return redirect()->route('admin.banner.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Banner has been deleted'
            ]);
    }
}
