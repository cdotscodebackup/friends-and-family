<?php

namespace App\Http\Controllers\Backend;


use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\Staff;
use App\Models\User;
use App\Models\OrderReceivedAmount;
use App\Models\ProviderSendPayment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class WalletController extends Controller
{

    public function index()
    {
        $recived_amount                 = OrderReceivedAmount::sum('received_amount');
        $give_amount                    = OrderReceivedAmount::sum('give_amount');
        $total_uncompleted_order_amount = OrderReceivedAmount::whereHas('order_amount',function($q){
            $q->where('order_status','!=','completed')->where('staff_status','!=','completed')->where('payment_status', 'paid');})->sum('received_amount');
        if($total_uncompleted_order_amount == null){
            $total_uncompleted_order_amount=0;
        }
        $wallet_amount  = ($recived_amount - $total_uncompleted_order_amount) - $give_amount;
        $staffs           = User::has('order_staff')->get();
        $staffs->each(function($staff, $key) use($staffs) {
            $staffs[$key]->order  = Order::where('staff_id',$staff->id)->where('order_status','completed')->where('staff_status','completed')->sum('staff_commission');
            $staffs[$key]->provider_received_amount	 = OrderReceivedAmount::whereHas('order_amount',function($q) use($staff){
                $q->where('staff_id',$staff->id)->where('order_status','completed')->where('staff_status','completed');})->sum('provider_received_amount');
            $staffs[$key]->provider_give_amount	 = OrderReceivedAmount::whereHas('order_amount',function($q) use($staff){
                    $q->where('staff_id',$staff->id)->where('order_status','completed')->where('staff_status','completed');})->sum('provider_give_amount');
            });
        return view('backend.wallet.index', compact('recived_amount','give_amount','wallet_amount','staffs','total_uncompleted_order_amount'));
    }

    public function staffOrder($id)
    {
        $get_all_payment_received_by_provider = ProviderSendPayment::where('status','verified')->sum('total_amount');
        if($get_all_payment_received_by_provider == null){
            $get_all_payment_received_by_provider=0;
        }
        $get_all_provider_given_amount = OrderReceivedAmount::whereHas('order_amount',function($q) use($id){
            $q->where('staff_id',$id)->where('order_status','completed')->where('staff_status','completed');})->sum('provider_give_amount');
        if($get_all_provider_given_amount == null){
            $get_all_provider_given_amount = 0;
        }
        
        $get_all_payment_received_by_company = OrderReceivedAmount::whereHas('order_amount',function($q) use($id){
            $q->where('staff_id',$id)->where('order_status','completed')->where('staff_status','completed');})->sum('received_amount');
        if($get_all_payment_received_by_company == null){
            $get_all_payment_received_by_company=0;
        }
        $get_all_company_given_amount = OrderReceivedAmount::whereHas('order_amount',function($q) use($id){
            $q->where('staff_id',$id)->where('order_status','completed')->where('staff_status','completed');})->sum('give_amount');
        if($get_all_company_given_amount == null){
            $get_all_company_given_amount = 0;
        }
        $get_company_share = Order::where('staff_id',$id)->where('payment_status', 'paid')->where('order_status','completed')->where('staff_status','completed')->sum('company_commission');
        if($get_company_share == null){
                $get_company_share = 0;
        }
        $Orders  = Order::with('staff')->where('staff_id',$id)->where('order_status','completed')->where('staff_status','completed')->get();
        $Orders->each(function($Order, $key) use($Orders) {
            $Orders[$key]->provider_received_amount = OrderReceivedAmount::where('order_id',$Order->id)->sum('provider_received_amount');
            $Orders[$key]->provider_give_amount = OrderReceivedAmount::where('order_id',$Order->id)->sum('provider_give_amount');
            $Orders[$key]->received_amount = OrderReceivedAmount::where('order_id',$Order->id)->sum('received_amount');
            });
        return view('backend.wallet.staff_order', compact('Orders','get_all_payment_received_by_provider','get_all_provider_given_amount','get_all_payment_received_by_company','get_all_company_given_amount','get_company_share'));
    }

    public function storepayment(Request $request){
        $date = \Carbon\Carbon::today();
        if($request->provider_give_amount  == 0 && $request->give_amount == 0 ){
            return redirect()->route('admin.wallet.index')
            ->with([
                'flash_status' => 'error',
                'flash_message' => 'Please enter at least one amount.'
            ]);
        }
        else{

        $order_received_amount                           = new OrderReceivedAmount();
        $order_received_amount->order_id                 = $request->order_id;
        $order_received_amount->received_amount          = (int)$request->provider_give_amount!=0 ? (int)$request->provider_give_amount : 0;
        $order_received_amount->give_amount              = (int)$request->give_amount;
        $order_received_amount->provider_received_amount = (int)$request->give_amount !=0 ? (int)$request->give_amount : 0;
        $order_received_amount->provider_give_amount     = (int)$request->provider_give_amount;
        $order_received_amount->description              = 'staff';
        $order_received_amount->save();
        
        $order  = Order::where('id',$request->order_id)->first();
        $provider_received_amount = OrderReceivedAmount::where('order_id',$request->order_id)->sum('provider_received_amount');
        $provider_give_amount = OrderReceivedAmount::where('order_id',$request->order_id)->sum('provider_give_amount');
        $received_amount = OrderReceivedAmount::where('order_id',$request->order_id)->sum('received_amount');
        $give_amount = OrderReceivedAmount::where('order_id',$request->order_id)->sum('give_amount');
        if($order->company_commission == ($received_amount - $give_amount) && $order->staff_commission == ($provider_received_amount - $provider_give_amount)){
          $order->payment_status = "paid";
          $order->save();  
          $get_order = Order::where('order_status', 'completed')->where('payment_status', 'unpaid')->where('staff_id',$order->staff_id)->first();
          if($get_order){
            $user = User::where('id', $order->staff_id)->update([
                'profile_status' => 'defaulter'
            ]);
          }
          else{
            $user = User::where('id', $order->staff_id)->update([
                'profile_status' => 'active'
            ]);
          }
        }
        else{
        }

        return redirect()->route('admin.wallet.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Payment add successfully.'
            ]);
        }
    }
}
