<?php

namespace App\Http\Controllers\Backend;


use App\Http\Controllers\Controller;
use App\Models\ServiceCategory;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\Category;
use App\Models\Staff;
use App\Models\StaffServices;
use App\Models\SubCategory;
use App\Models\User;
use App\Models\Notification;
use App\Models\UserReferral;
use App\Traits\GeneralHelperTrait;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Helpers\SendSms;


class OrderController extends Controller
{

    use GeneralHelperTrait;

    public function index(Request $request)
    {

        $orders = Order::whereNotNull('id');

        if (!is_null($request->status)) {

            $orders->where('order_status', $request->status);

        }

        if (!is_null($request->start_date)) {

            $orders->whereDate('created_at', '>=', $request->start_date);

        }

        if (!is_null($request->end_date)) {

            $orders->whereDate('created_at', '<=', $request->end_date);
        }

        $orders = $orders->get();

        $staffs = User::where('user_type', 'staff')->get();
        return view('backend.orders.index', compact('orders', 'staffs'));
    }

    public function showOrder($id)
    {
        $order = Order::findOrFail($id);
        return view('backend.orders.show', compact('order'));

    }

    public function create()
    {

        $categories = Category::all();
        $employees = User::where('user_type', 'customer')->where('status', '!=', 'suspended')->get();

        return view('backend.orders.create', compact('categories', 'employees'));
    }

    public function store(Request $request)
    {
        $orderId =0;

        if (!is_null($request->customer_id)) {

            $alreadySelectedCategory = 0;
            $alreadySelectedService = 0;
            if (!empty($request->selected_category_id)) {

                foreach ($request->get('selected_category_id') as $key => $category) {
                    if ($alreadySelectedCategory != $category) {
                        if (!empty($request->get('selected_service_id')[$category])) {
                            foreach ($request->get('selected_service_id')[$category] as $index => $service) {

                                if ($alreadySelectedService != $service) {

                                    if (!empty($request->get('selected_menu_item_id')[$category][$service])) {
                                        foreach ($request->get('selected_menu_item_id')[$category][$service] as $ith => $menu_item) {
                                            $order_id = $this->orderNumber();
                                            $order = Order::create([

                                                'customer_id' => $request->customer_id,
                                                'order_id' => $order_id,
                                                'area_id' => $request->area_id,
                                                'city_id' => $request->city_id,
                                                'address' => $request->address,
                                                'phone_number' => $request->mobile_number,
                                                'total_price' => (int)$request->net_price,
                                                'order_status' => 'pending',
                                                'staff_status' => 'pending',
                                                'delivery_charges' => $request->delivery_charges,
                                                'time_zone' => $request->time_zone,
                                                'order_type' => $request->selected_category_name,
                                                'payment_mode' => $request->payment_mode,

                                            ]);
                                            $orderId = $order->order_id;

                                            $menuItem = SubCategory::where('id', $menu_item)->first();
                                            $order_details = OrderDetail::create([
                                                'order_id' => $order->id,
                                                'category_id' => (int)$category,
                                                'service_id' => (int)$service,
                                                'menu_item_id' => (int)$menu_item,
                                                'name' => $menuItem->name,
                                                'duration' => ($request->get('selected_menu_item_type')[$category][$service][$menu_item] == 'Hourly') ? $request->duration * 60 : 0,
                                                'amount' => (int)$request->net_price,
                                                'date' => Carbon::parse(strtotime($request->get('datetimepicker1'))),
                                                'time' => Carbon::parse($request->get('time')),
                                                'alternate_address' => $request->get('selected_service_alternate_address')[$category][$service][$menu_item] ?? $request->address,
                                                'type' => (!is_null($request->get('selected_menu_item_type')[$category][$service][$menu_item])) ? $request->get('selected_menu_item_type')[$category][$service][$menu_item] : 'Fixed',
                                            ]);
                                        }
                                    }
                                    $get_service = ServiceCategory::where('id',$service)->first();
                                    if($get_service){
                                        $get_service->top_service = $get_service->top_service + 1;
                                        $get_service->save(); 
                                    }

                                }
                                $alreadySelectedService = (int)$service;
                            }
                        }
                        $alreadySelectedCategory = (int)$category;
                    }

                }
            }
            $user = User::where('id',$request->customer_id)->first();
            $remove_zero = ltrim($user->phone_number, '0');
            $add_number = '92';
            $phone_number = $add_number . $remove_zero;
            $message = 'Thank you for booking an appointment with Friends & Family. Someone from our team will be calling you shortly to confirm your appointment.Your Order ID is::' . $orderId . '';
//            event(new SendReferralCodeWithPhone($user_name = '', $phone_number, $message));
             $notification = new Notification;
             $notification->user_id      = $user->id;
             $notification->message_type = 'order';
             $notification->message      = $message;
             $notification->save();

            $response['data'] = [
                'flash_status' => 'success',
                'flash_message' => 'Order Created Successfully.',
            ];

        } else {
            $response['data'] = [
                'flash_status' => 'error',
                'flash_message' => 'Something Went Wrong.Please Try Again.',
            ];
        }
        return $response;
    }

    public function editOrder($id)
    {

        $order = Order::whereId($id)->first();
        $orderDetail = OrderDetail::where('order_id', $id)->first();
        $employees = User::where('user_type', 'customer')->get();
        $categories = Category::all();
        $services = ServiceCategory::all();
        $menuItems = SubCategory::all();
        $categoryIds = OrderDetail::whereOrderId($order->id)->pluck('category_id')->toArray();
        $serviceIds = OrderDetail::whereOrderId($order->id)->pluck('service_id')->toArray();
        $menuItemsIds = OrderDetail::whereOrderId($order->id)->pluck('menu_item_id')->toArray();


        $discount = 0;

        $coupon_discount = $order->coupon_discount ?? 0;
        $membership_discount = $order->membership_discount ?? 0;
        $first_order_discount = $order->first_order_discount ?? 0;
        $referral_discount = $order->referral_discount ?? 0;
        $discount = $coupon_discount + $membership_discount + $first_order_discount + $referral_discount;
        $todaysOrders = null;
        if (!is_null($serviceIds[0])) {

            $staffIDs = StaffServices::whereIn('service_category_id', $serviceIds)->pluck('staff_id')->toArray();
            $staffs_users = Staff::whereIn('id', $staffIDs)->pluck('user_id')->toArray();
            $staffs = User::whereIn('id', $staffs_users)->where('user_type', 'staff')->get();


            $todaysOrders = DB::table('orders')
                ->select('staff_id', DB::raw('count(staff_id) as currentOrders'))
                ->groupBy('staff_id')
                ->whereDate('created_at', Carbon::today())
                ->get();

        } else {
            $staffs = User::where('user_type', 'staff')->get();
        }

        return view('backend.orders.edit', compact('order', 'orderDetail', 'employees', 'categories', 'services', 'menuItems', 'serviceIds', 'categoryIds', 'menuItemsIds', 'staffs', 'discount', 'todaysOrders'));
    }

    public function updateOrder(Request $request)
    {

        if (!is_null($request->customer_id)) {

            Order::whereId($request->order_id)->update([

                'customer_id' => $request->customer_id,
                'area_id' => $request->area_id,
                'city_id' => $request->city_id,
                'address' => $request->address,
                'phone_number' => $request->mobile_number,
                'total_price' => $request->net_price,
                'order_status' => $request->order_status,
                'staff_status' => 'pending',
                'delivery_charges' => $request->delivery_charges,
                'time_zone' => $request->time_zone,
                'staff_id' => $request->staff_id,
                'order_type' => $request->selected_category_name,
                'payment_mode' => $request->payment_mode,

            ]);


            $order_detail_id = OrderDetail::whereOrderId($request->order_id)->get();
            OrderDetail::whereOrderId($request->order_id)->delete();


            $alreadySelectedCategory = 0;
            $alreadySelectedService = 0;
            if (!empty($request->selected_category_id)) {

                foreach ($request->get('selected_category_id') as $key => $category) {
                    if ($alreadySelectedCategory != $category) {
                        if (!empty($request->get('selected_service_id')[$category])) {
                            foreach ($request->get('selected_service_id')[$category] as $index => $service) {

                                if ($alreadySelectedService != $service) {

                                    if (!empty($request->get('selected_menu_item_id')[$category][$service])) {
                                        foreach ($request->get('selected_menu_item_id')[$category][$service] as $ith => $menu_item) {
                                            $menuItem = SubCategory::where('id', $menu_item)->first();
                                            $order_details = OrderDetail::create([
                                                'order_id' => $request->order_id,
                                                'category_id' => (int)$category,
                                                'service_id' => (int)$service,
                                                'menu_item_id' => (int)$menu_item,
                                                'name' => $menuItem->name,
                                                'duration' => (!is_null($request->get('selected_menu_item_type')[$category][$service][$menu_item])) ? ($request->get('selected_menu_item_type')[$category][$service][$menu_item] == 'Hourly') ? $request->duration * 60 : 0 : 0,
                                                'amount' => (int)$request->net_price,
                                                'date' => Carbon::parse(strtotime($request->get('datetimepicker1'))),
                                                'time' => Carbon::parse($request->get('time')),
                                                'alternate_address' => $request->get('selected_service_alternate_address')[$category][$service][$menu_item] ?? $request->address,
                                                'type' => (!is_null($request->get('selected_menu_item_type')[$category][$service][$menu_item])) ? $request->get('selected_menu_item_type')[$category][$service][$menu_item] : 'Fixed',


                                            ]);
                                        }
                                    }
                                }
                                $alreadySelectedService = (int)$service;
                            }
                        }
                        $alreadySelectedCategory = (int)$category;
                    }

                }
            }

            if (!is_null($request->staff_id)) {
                $staff = User::where('id', $request->staff_id)->first();
                $customer = User::where('id', $request->customer_id)->first();
                $remove_zero = ltrim($staff->phone_number, '0');
                $add_number = '92';
                $phone_number = $add_number . $remove_zero;

                $remove_zero1 = ltrim($customer->phone_number, '0');
                $add_number1 = '92';
                $phone_number1 = $add_number1 . $remove_zero1;

                $message = 'Your appointment is confirmed for ' . $request->datetimepicker1 . ' with ' . strtoupper($staff->fullName()) . '. Thank you for using Friends & Family!';
//                event(new SendReferralCodeWithPhone($user_name = '', $phone_number1, $message));
                $staff_message = 'Your appointment is confirmed for ' . $request->datetimepicker1 . ' with ' . strtoupper($staff->fullName()) . '. Thank you for using Friends & Family!';
//                event(new SendReferralCodeWithPhone($user_name = '', $phone_number, $message));

             $notification = new Notification;
             $notification->user_id      = $customer->id;
             $notification->message_type = 'order';
             $notification->message      = $message;
             $notification->save();

             $staff_notification = new Notification;
             $staff_notification->user_id      = $staff->id;
             $staff_notification->message_type = 'order';
             $staff_notification->message      = $staff_message;
             $staff_notification->save();
            }
            else{
                if($request->order_status == "confirmed"){
                    $customer = User::where('id', $request->customer_id)->first();
                    $remove_zero = ltrim($customer->phone_number, '0');
                    $add_number = '92';
                    $phone_number = $add_number . $remove_zero;

                    $message = 'Your appointment is confirmed for ' . $request->datetimepicker1 .'. Thank you for using Friends & Family!';
//                event(new SendReferralCodeWithPhone($user_name = '', $phone_number, $message));
                    $notification = new Notification;
                    $notification->user_id      = $customer->id;
                    $notifiation->message_type = 'order';
                    $notification->message      = $message;
                    $notification->save();
                }
            }


            $response['data'] = [
                'flash_status' => 'success',
                'flash_message' => 'Order Updated Successfully.',
            ];

        } else {
            $response['data'] = [
                'flash_status' => 'error',
                'flash_message' => 'Something Went Wrong.Please Try Again.',
            ];
        }
        return $response;

    }

    public function destroy($id)
    {
        $order = Order::findOrFail($id);
        $order->delete();

        return redirect()->route('admin.get.order.history')->with([
            'flash_status' => 'success',
            'flash_message' => 'Order Deleted Successfully.',
        ]);
    }

    public function uploadpaymentslipbyadmin(Request $request)
    {
        $order = Order::findOrFail($request->order_id);
        $deposit_slip_path = null;
//        dd($request->all());
        if ($request->has('payment_slip_image')) {
//            $image = $request->file('payment_slip_image');
//            $name = $image->getClientOriginalName();
//            $name = str_replace(" ", "", $name);
//            $destinationPath = public_path('/uploads/deposit_slips');
//            $imagePath = $destinationPath . "/" . $name;
//            $image->move($destinationPath, $name);
//            $deposit_slip_path = $name;
            $deposit_slip_path = $request->get('payment_slip');


        } else {
            $deposit_slip_path = null;
        }
        $order->payment_slip_image = $deposit_slip_path;
        $order->save();
        return redirect()->route('admin.get.order.history')->with([
            'flash_status' => 'success',
            'flash_message' => 'Order Payment slip add Successfully.',
        ]);
    }

}
