<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;

use App\Models\Commisions;
use Illuminate\Http\Request;

class CommisionsController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:admin');
    }

    public function index()
    {
        $comissions = Commisions::orderBy('id', 'DESC')->get();
        return view('backend.comissions.index', compact('comissions'));
    }

    public function create()
    {
        return view('backend.comissions.create');

    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'company_comission' => 'required',
            'staff_comission' => 'required',
        ], [
            'title.required' => 'Title is required',
            'company_comission.required' => 'Company Comission Percentage is required',
            'staff_comission.required' => 'Staff Comission Percentage is required'
        ]);

        Commisions::create($request->all());
        return redirect()->route('admin.comissions.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Comission created successfully.'
            ]);

    }

    public function edit($id)
    {
        $comission = Commisions::whereId($id)->first();
        return view('backend.comissions.edit', compact('comission'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'company_comission' => 'required',
            'staff_comission' => 'required',
        ], [
            'title.required' => 'Title is required',
            'company_comission.required' => 'Company Comission Percentage is required',
            'staff_comission.required' => 'Staff Comission Percentage is required'
        ]);

        $comission = Commisions::whereId($id)->first();
        $comission->fill($request->all());
        $comission->save();
        return redirect()->route('admin.comissions.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Comission updated successfully.'
            ]);

    }

    public function destroy($id)
    {
        $comission = Commisions::findOrFail($id);
        $comission->delete();

        return redirect()->route('admin.comissions.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Comission has been deleted'
            ]);
    }
}
