<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\ServiceCategory;
use App\Models\PivotServiceCategory;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Intervention\Image\ImageManagerStatic as Image;


class ServiceCategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:admin');
    }

    public function index()
    {
        $service_categories = ServiceCategory::with('category')->orderBy('id', 'DESC')->get();
        return view('backend.service_category.index', compact('service_categories'));
    }

    public function create()
    {
        $categories = Category::all();
        return view('backend.service_category.create', compact('categories'));

    }

    public function show($id)
    {

    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'image' => 'required|mimes:jpg,jpeg,png|max:2048',
            'thumbnail_image' => 'required|mimes:jpg,jpeg,png|max:2048',
            'icon' => 'required|mimes:jpg,jpeg,png|max:2048',
            'slug' => 'required|unique:service_categories,slug',
            'category_id.0' => 'required',
            'summary' => 'required',
            'description' => 'required',
//            'price' => 'required|integer|min:1',
        ], [
            'category_id.0.required' => 'At least one Category is required.',
            'image.required' => 'Image  is required.',
            'thumbnail_image.required' => 'Thumbnail Image  is required.',
            'icon.required' => 'Icon Image  is required.',
//            'price.required' => 'Category Price is Required.',
        ]);

        if ($request->has('image')) {
            $image = $request->file('image');
            $name = $image->getClientOriginalName();
            $destinationPath = public_path('/uploads/service_category');
            $imagePath = $destinationPath . "/" . $name;
            $image->move($destinationPath, $name);
            $profile_image ='https://familyfriends.pk/uploads/service_category/' . $name;


        } else {
            $profile_image = null;
        }


        if ($request->has('thumbnail_image')) {
            $image_new = $request->file('thumbnail_image');
            $name_new = $image_new->getClientOriginalName();
            $destinationPathThumbail = public_path('/uploads/service_category/thumbnails');
            $imagePathNre = $destinationPathThumbail . "/" . $name_new;
            $image_new->move($destinationPathThumbail, $name_new);
            $thumbnail_image ='https://familyfriends.pk/uploads/service_category/thumbnails/' . $name_new;

        } else {
            $thumbnail_image = null;
        }

        if ($request->has('icon')) {
            $image_new_icon = $request->file('icon');
            $name_new_icon = $image_new_icon->getClientOriginalName();
            $destinationPathIcon = public_path('/uploads/service_category/icons');
            $imagePathNreIcon = $destinationPathIcon . "/" . $name_new_icon;
            $image_new_icon->move($destinationPathIcon, $name_new_icon);
            $icon_image ='https://familyfriends.pk/uploads/service_category/icons/' . $name_new_icon;

        } else {
            $icon_image = null;
        }

        $service_category = new ServiceCategory();
        $service_category->name             = $request->name;
        $service_category->slug             = $request->slug;
        $service_category->price            = 0;
        $service_category->image            = $profile_image;
        $service_category->thumbnail_image  = $thumbnail_image;
        $service_category->icon             = $icon_image;
        $service_category->summary          = $request->summary;
        $service_category->description      = $request->description;
        $service_category->discount_type    = "";
        $service_category->discount_price   = 0;
        $service_category->meta_title       = $request->meta_title;
        $service_category->meta_description = $request->meta_description;
        $service_category->save();
        foreach ($request->category_id as $category){
           $pivot_service_category =new  PivotServiceCategory();
           $pivot_service_category->category_id = $category;
           $pivot_service_category->service_id = $service_category->id;
           $pivot_service_category->save();
        }
        return redirect()->route('admin.services.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Service created successfully.'
            ]);

    }

    public function edit($id)
    {
        $service_category = ServiceCategory::with('category')->where('id',$id)->first();
        $categories = Category::all();
        return view('backend.service_category.edit', compact('service_category', 'categories'));
    }

    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'name' => 'required',
            'slug' => 'required',
            'category_id.0' => 'required',
            'image' => 'mimes:jpg,jpeg,png|max:2048',
            'thumbnail_image' => 'mimes:jpg,jpeg,png|max:2048',
            'summary' => 'required',
            'description' => 'required',
//            'price' => 'required|integer|min:1',

        ], [
            'category_id.0.required' => 'Category is required.',
            'image.required' => 'Image  is required.',
            'thumbnail_image.required' => 'Thumbnail Image  is required.',
//            'price.required' => 'Category Price is Required.',
        ]);

        $category = ServiceCategory::find($id);
        if ($request->has('image')) {
            $image = $request->file('image');

            $name = $image->getClientOriginalName();
            $destinationPath = public_path('/uploads/service_category');
            $imagePath = $destinationPath . "/" . $name;
            $image->move($destinationPath, $name);
            $profile_image ='https://familyfriends.pk/uploads/service_category/' . $name;


        } else {
            $profile_image = $category->image;
        }


        if ($request->has('thumbnail_image')) {

            $image_new = $request->file('thumbnail_image');
            $name_new = $image_new->getClientOriginalName();
            $destinationPathThumbail = public_path('/uploads/service_category/thumbnails');
            $imagePathNre = $destinationPathThumbail . "/" . $name_new;
            $image_new->move($destinationPathThumbail, $name_new);
            $thumbnail_image ='https://familyfriends.pk/uploads/service_category/thumbnails/' . $name_new;


        } else {
            $thumbnail_image = $category->thumbnail_image;
        }

        if ($request->has('icon')) {
            $image_new_icon = $request->file('icon');
            $name_new_icon = $image_new_icon->getClientOriginalName();
            $destinationPathIcon = public_path('/uploads/service_category/icons');
            $imagePathNreIcon = $destinationPathIcon . "/" . $name_new_icon;
            $image_new_icon->move($destinationPathIcon, $name_new_icon);
            $icon_image ='https://familyfriends.pk/uploads/service_category/icons/' . $name_new_icon;

        } else {
            $icon_image = $category->icon;
        }


        if ($request->has('discount_type') && $request->discount_type == 'fixed') {
            $price = $request->discount_price;
            $discount = $request->discount_type;

        } elseif ($request->has('discount_type') && $request->discount_type == 'percentage') {
//            $price = (int)ceil($request->discount_price / 100);
            $price = $request->discount_price;
            $discount = $request->discount_type;
        } else {
            $price = 0;
            $discount = null;
        }

        $category->update([
            'name'               => $request->name,
            'slug'               => $request->slug,
//            'price' => $request->price,
            'image'              => $profile_image,
            'thumbnail_image'    => $thumbnail_image,
            'icon'               => $icon_image,
            'summary'            => $request->summary,
            'description'        => $request->description,
            'discount_type'      => $discount,
            'discount_price'     => $price,
            'meta_title'         => $request->meta_title,
            'meta_description'   => $request->meta_description,
            'meta_keywords'      => $request->meta_keywords,
        ]);
        $get_pivot_services = PivotServiceCategory::where('service_id',$id)->get();
        foreach($get_pivot_services as $services){
            $services->delete();
        }
        foreach ($request->category_id as $category){
            $pivot_service_category =new  PivotServiceCategory();
            $pivot_service_category->category_id = $category;
            $pivot_service_category->service_id = $id;
            $pivot_service_category->save();
         }

        return redirect()->route('admin.services.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Service updated successfully.'
            ]);

    }

    public function destroy($id)
    {
        $service_category = ServiceCategory::findOrFail($id);
        $service_category->delete();
        $get_pivot_services = PivotServiceCategory::where('service_id',$id)->get();
        foreach($get_pivot_services as $services){
            $services->delete();
        }
        return redirect()->route('admin.services.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Service has been deleted'
            ]);
    }
}
