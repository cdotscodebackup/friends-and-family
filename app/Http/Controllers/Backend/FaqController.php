<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Faq;
use Illuminate\Http\Request;

class FaqController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:admin');
    }

    public function index()
    {
        $faqs = Faq::orderBy('id', 'DESC')->get();
        return view('backend.faqs.index', compact('faqs'));
    }

    public function create()
    {
        return view('backend.faqs.create');

    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
        ], [
            'title.required' => 'Title is required',
            'description.required' => 'Description is required'
        ]);


        $faqs = Faq::all();

        if (count($faqs) > 4) {
            return redirect()->route('admin.faqs.index')
                ->with([
                    'flash_status' => 'error',
                    'flash_message' => 'You can only add 5 faqs'
                ]);
        } else {
            Faq::create($request->all());
            return redirect()->route('admin.faqs.index')
                ->with([
                    'flash_status' => 'success',
                    'flash_message' => 'Faq created successfully.'
                ]);
        }


    }

    public function edit($id)
    {
        $faq = Faq::whereId($id)->first();
        return view('backend.faqs.edit', compact('faq'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [

            'title' => 'required',
            'description' => 'required',
        ], [
            'title.required' => 'Title is required',
            'description' => 'Description is required'
        ]);

        $other = Faq::whereId($id)->first();
        $other->fill($request->all());
        $other->save();
        return redirect()->route('admin.faqs.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Faq updated successfully.'
            ]);

    }

    public function destroy($id)
    {
        $faq = Faq::findOrFail($id);
        $faq->delete();

        return redirect()->route('admin.faqs.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Faq has been deleted'
            ]);
    }
}
