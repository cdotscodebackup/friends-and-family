<?php

namespace App\Http\Controllers\Frontend;

use App\Events\SendReferralCodeWithPhone;
use App\Http\Controllers\Controller;
use App\Models\Area;
use App\Models\Category;
use App\Models\City;
use App\Models\Coupon;
use App\Models\CouponUser;
use App\Models\FirstOrderDiscount;
use App\Models\MembershipDiscount;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\OrderMenuItemAddon;
use App\Models\ReferralDiscount;
use App\Models\ServiceCategory;
use App\Models\SubCategory;
use App\Models\UserReferral;
use App\Models\Notification;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Traits\GeneralHelperTrait;
use Illuminate\Support\Facades\Auth;
use App\Helpers\SendSms;

class OrderController extends Controller
{
    use GeneralHelperTrait;


    public function index($category, $slug, $subcategory)
    {
        if (Auth::check()) {
            if (auth()->user()->hasRole('customer')) {
                $service = ServiceCategory::where('slug', $slug)->first();
                $category = Category::where('name', $category)->first();
                $menuItem = SubCategory::where('id', $subcategory)->first();
                $user = Auth::user();
                return view('frontend.pages.orders.order', compact('service', 'menuItem', 'user', 'category'));
            } else {
                return redirect()->back()->with(['flash_status' => 'error', 'flash_message' => 'You are unauthorized for this request']);
            }
        } else {
            return redirect()->route('login')->with(['flash_status' => 'error', 'flash_message' => 'Please login for booking a Appointment.']);
        }

    }


    public function orderDetail($category, $slug, $menuItem, Request $request)
    {
//        if ($request->ajax()) {
//            if (Auth::check()) {
//                if (auth()->user()->hasRole('customer')) {
//                    return response()->json(['flash_status' => 'success', 'flash_message' => '']);
//                } else {
//                    return response()->json(['flash_status' => 'error', 'flash_message' => 'You are authorized for this request']);
//                }
//            } else {
//                return redirect()->route('login')->with(['flash_status' => 'error', 'flash_message' => 'Please login for booking a Appointment.']);
//            }
//        } else {

        if (Auth::check()) {
            if (auth()->user()->hasRole('customer')) {
                $service = ServiceCategory::where('slug', $slug)->first();
                $category = Category::where('name', $category)->first();
                $menuItem = SubCategory::where('id', $menuItem)->first();
                $user = Auth::user();

                return view('frontend.pages.orders.order_details', compact('service', 'menuItem', 'user', 'category'));
            } else {
                return redirect()->back()->with(['flash_status' => 'error', 'flash_message' => 'You are authorized for this request']);
            }
        } else {
            return redirect()->route('login')->with(['flash_status' => 'error', 'flash_message' => 'Please login for booking a Appointment.']);
        }


//        }

    }

    public function orderReview()
    {
        $user = auth()->user();
        $city_name = City::where('id', $user->city_id)->first()->name;
        $areas = Area::where('id', $user->area_id)->get();

        $referral_users = UserReferral::where('user_id', $user->id)->where('status', 'pending')->first();

        if (!is_null($referral_users)) {
            $referral_discount = ReferralDiscount::first();
            $referral_user_id = $referral_users->id;
            if (isset($user->membership_id)) {
                $membership_discount = MembershipDiscount::where('id', $user->membership_id)->first();
                return view('frontend.pages.orders.order-review', compact('user', 'city_name', 'areas', 'membership_discount'));

            } else {
                return view('frontend.pages.orders.order-review', compact('user', 'city_name', 'areas'));

            }


        }
        if ($user->orders()->count() == 0) {
            $first_order_discount = FirstOrderDiscount::first();
            if (isset($user->membership_id)) {
                $membership_discount = MembershipDiscount::where('id', $user->membership_id)->first();
                return view('frontend.pages.orders.order-review', compact('user', 'city_name', 'areas', 'membership_discount', 'first_order_discount'));

            } else {
                return view('frontend.pages.orders.order-review', compact('user', 'city_name', 'areas', 'first_order_discount'));

            }

        }
        if (isset($user->membership_id)) {
            $membership_discount = MembershipDiscount::where('id', $user->membership_id)->first();
            return view('frontend.pages.orders.order-review', compact('user', 'city_name', 'areas', 'membership_discount'));

        } else {
            return view('frontend.pages.orders.order-review', compact('user', 'city_name', 'areas'));

        }

    }

    public function finalOrder(Request $request)
    {
        if (!empty($request->finalOrder)) {
            $order_id = $this->orderNumber();
            $user = Auth::user();
            $order = Order::create([


                'customer_id' => $user->id,
                'order_id' => $order_id,
                'area_id' => $user->area_id,
                'city_id' => $user->city_id,
                'address' => $user->address,
                'phone_number' => $user->phone_number,
                'order_duration' => (int)$request->finalOrder['duration'] * 60,
                'total_price' => $request->finalOrder['totalPrice'],
                'grand_total' => $request->finalOrder['totalPrice'],
                'order_status' => 'pending',
                'staff_status' => 'pending',
                'alternate_address' => $request->finalOrder['alternateAddress'],
                'delivery_charges' => ($request->finalOrder['order_type'] == 'online ') ? 0 : auth()->user()->area->price,
                'time_zone' => $request->finalOrder['timeZone'],
                'order_type' => $request->finalOrder['order_type'],
                'payment_mode' => $request->finalOrder['payment_mode'],
                'payment_slip_image' => $request->finalOrder['payment_slip'],

            ]);

            if (!is_null($request->orderDiscount['referral_discount']) && !empty($request->orderDiscount['referral_discount'])) {

                $user_referrals = UserReferral::where('user_id', auth()->user()->id)->where('status', 'pending')->first();
                $user_referrals->status = 'taken';
                $user_referrals->save();

            }
            $order_details = OrderDetail::create([
                'order_id' => $order->id,
                'category_id' => $request->finalOrder['category_id'],
                'service_id' => $request->finalOrder['service_id'],
                'menu_item_id' => $request->finalOrder['menu_item_id'],
                'name' => $request->finalOrder['name'],
                'amount' => $request->finalOrder['price'],
                'duration' => (int)$request->finalOrder['duration'] * 60,
                'type' => $request->finalOrder['service_type'],
                'time' => Carbon::parse($request->finalOrder['time']),
                'date' => Carbon::parse(strtotime($request->finalOrder['requestedDatetime'])),
                'alternate_address' => $request->finalOrder['alternateAddress'],
            ]);
            $get_service = ServiceCategory::where('id',$order_details->service_id)->first();
            if($get_service){
              $get_service->top_service = $get_service->top_service + 1;
              $get_service->save(); 
            }
            $remove_zero = ltrim(auth()->user()->phone_number, '0');
            $add_number = '92';
            $phone_number = $add_number . $remove_zero;
            $message = 'Thank you for booking an appointment with Friends & Family. Someone from our team will be calling you shortly to confirm your appointment.Your Order ID is::' . $order_id . '';
//            event(new SendReferralCodeWithPhone($user_name = '', $phone_number, $message));
             $notification = new Notification;
             $notification->user_id      = $user->id;
             $notification->message_type = 'order';
             $notification->message      = $message;
             $notification->save();

            $response['data'] = [
                'flash_status' => 'success',
                'flash_message' => 'Order Created Successfully.',
                'order_id' => $order->id
            ];

        } else {
            $response['data'] = [
                'flash_status' => 'error',
                'flash_message' => 'Something Went Wrong.Please Try Again.',
            ];

        }
        return $response;

    }
}
