<?php

namespace App\Http\Controllers\ServiceProvider;


use App\Http\Controllers\Controller;
use App\Models\Order;
use Illuminate\Http\Request;
use Auth;


class LedgerController extends Controller
{

    public function index()
    {
        $orders =Order::with(['user','staff.comission'])->where('order_status','completed')->where('staff_status','completed')->where('staff_id',Auth::user()->id)->get();
        return view('service-provider.ledger.index', compact('orders'));
    }

}
