<?php

namespace App\Http\Controllers\ServiceProvider;


use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\Staff;
use App\Models\User;
use App\Models\OrderReceivedAmount;
use App\Models\ProviderSendPayment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Intervention\Image\ImageManagerStatic as Image;
use Auth;

class WalletController extends Controller
{

    public function index()
    {
        $staff_id = Auth::user()->id;

        $provider_received_amount = OrderReceivedAmount::whereHas('order_amount.staff',function($q) use($staff_id){
            $q->where('id',$staff_id);})->sum('provider_received_amount');

        $provider_give_amount    = OrderReceivedAmount::whereHas('order_amount.staff',function($q) use($staff_id){
            $q->where('id',$staff_id);})->sum('provider_give_amount');

        $wallet_provider_amount  = $provider_received_amount - $provider_give_amount;

        $company_share_amount    = Order::where('staff_id',$staff_id)->where('order_status','completed')->where('staff_status','completed')->sum('company_commission');

        $received_amount	     = OrderReceivedAmount::whereHas('order_amount.staff',function($q) use($staff_id){
            $q->where('id',$staff_id);})->sum('received_amount');

        $give_amount	         = OrderReceivedAmount::whereHas('order_amount.staff',function($q) use($staff_id){
                $q->where('id',$staff_id);})->sum('give_amount');

        return view('service-provider.wallet.index', compact('provider_received_amount','provider_give_amount','wallet_provider_amount','company_share_amount','received_amount','give_amount','staff_id'));
    }

    public function companyOrder($id)
    {
        $Orders  = Order::with('staff')->where('staff_id',$id)->where('order_status','completed')->where('staff_status','completed')->get();
        $Orders->each(function($Order, $key) use($Orders) {
            $Orders[$key]->received_amount = OrderReceivedAmount::where('order_id',$Order->id)->sum('received_amount');
            $Orders[$key]->give_amount     = OrderReceivedAmount::where('order_id',$Order->id)->sum('give_amount');
            });
        return view('service-provider.wallet.company_order', compact('Orders'));
    }

    public function sendpayment(Request $request){
        if($request->total_amount  == 0 ){
            return redirect()->route('staff.wallet.index')
            ->with([
                'flash_status' => 'error',
                'flash_message' => 'Please enter amount and deposit slip.'
            ]);
        }
        else{
            $deposit_slip_path = null;
            if ($request->has('deposit_slip')) {
                $image = $request->file('deposit_slip');
                $name = $image->getClientOriginalName();
                $name = str_replace(" ","",$name);
                $destinationPath = public_path('/uploads/deposit_slips');
                $imagePath = $destinationPath . "/" . $name;
                $image->move($destinationPath, $name);
                $deposit_slip_path ='https://familyfriends.pk/uploads/deposit_slips/' . $name;
    
    
            } else {
                $deposit_slip_path = null;
            }

        $provider_send_payment                      = new ProviderSendPayment();
        $provider_send_payment->staff_id            = Auth::user()->id;
        $provider_send_payment->total_amount        = (int)$request->total_amount;
        $provider_send_payment->deposit_slip_path   = $deposit_slip_path;
        $provider_send_payment->payment_mode        = $request->payment_mode;
        $provider_send_payment->save();
        return redirect()->route('staff.wallet.index')
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'Payment add successfully.'
            ]);
        }
    }
}
