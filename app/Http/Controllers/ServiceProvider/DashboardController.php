<?php

namespace App\Http\Controllers\ServiceProvider;

use App\Http\Controllers\Controller;
use App\Models\Order;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('role:staff');
    }

    public function index()
    {
        $total_orders = Order::where('staff_id', auth()->user()->id)->get()->count();
        $pending_orders = Order::where('staff_status', 'pending')->where('staff_id', auth()->user()->id)->get()->count();
        $accepted_orders = Order::where('staff_status', 'accepted')->where('staff_id', auth()->user()->id)->get();
        $completed_orders = Order::where('staff_status', 'completed')->where('staff_id', auth()->user()->id)->get();
        return view('service-provider.dashboard.index', compact('total_orders', 'pending_orders', 'completed_orders','accepted_orders'));
    }
}
